# Sisfonilai (Sistem Informasi Nilai)
Sisfonilai - Aplikasi sederhana ini adalah aplikasi berbasis web yang saya buat pada Ujian Kompetensi Keahlian 2015 (Paket Soal: 2). Aplikasi ini diprogram dengan bahasa/script sebagai berikut:
- PHP
- MySQL
- HTML
- CSS
- JavaScript

# Fitur
- Mendata data kompetensi keahlian.
- Mendata data mata diklat beserta standar kompetensinya.
- Mendata guru
- Mendata siswa
- Menginputkan nilai siswa per kelas (untuk guru)
- Backup database (untuk admin)
- Mencetak rapor (untuk siswa dan admin)

# Konfigurasi Awal
- Buat sebuah database MySQL dengan nama 22791_ukk.
- Impor file 22791_ukk.sql ke database barusan.
- Buka ~/app/functions.php dan edit fungsi baseUrl() & switchPage; edit bagian URL yang ada di sana menjadi url pada komputer anda.
- Buka URL aplikasi yang sudah anda set di komputer anda.

