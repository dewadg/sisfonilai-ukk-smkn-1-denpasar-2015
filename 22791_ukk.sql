-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 17 Mar 2015 pada 12.13
-- Versi Server: 5.6.14
-- Versi PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `22791_ukk`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `countScore`(IN `val` INT, IN `valsec` VARCHAR(5))
    NO SQL
SELECT
	ROUND(AVG(nilai_angka), 2) AS nilai
FROM
	nilai
INNER JOIN standar_kompetensi ON
	standar_kompetensi.kode_sk = nilai.kode_sk
INNER JOIN mata_diklat ON
	mata_diklat.kode_mata_diklat = standar_kompetensi.kode_mata_diklat
WHERE
	mata_diklat.kode_mata_diklat = val AND nilai.nisn = valsec$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectSiswa`(IN `val` VARCHAR(5))
    NO SQL
SELECT * FROM siswa WHERE nisn = val$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `nip` varchar(5) NOT NULL,
  `kode_kk` int(11) NOT NULL,
  `nama_guru` varchar(50) NOT NULL,
  `alamat_guru` text NOT NULL,
  `telp_guru` varchar(20) NOT NULL,
  `user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `user_type` enum('guru','','','') NOT NULL DEFAULT 'guru',
  `kode_mata_diklat` int(11) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`nip`, `kode_kk`, `nama_guru`, `alamat_guru`, `telp_guru`, `user`, `password`, `user_type`, `kode_mata_diklat`) VALUES
('66001', 5, 'Sadia Permana', 'Denpasar', '087860999888', 'sadia', 'sadia', 'guru', 5),
('66002', 2, 'Ida Bagus Oka', 'Mengwi', '081765787678', 'gusoka', 'gusoka', 'guru', 1),
('66003', 2, 'Dwi Jayanti', 'Sempidi', '0897890986543', 'dwijayanti', 'dwijayanti', 'guru', 2),
('66004', 2, 'Dodik Kustanto', 'Banyuwangi', '089787678789', 'dodik', 'dodik', 'guru', 3),
('66006', 5, 'Putri Suharsiki', 'Gianyar', '087676768788', 'putri', 'putri', 'guru', 6),
('66007', 2, 'Ngurah Bagus', 'Tabanan', '085678987687', 'ngurah', 'ngurah', 'guru', 7),
('66008', 7, 'Yoga Nugraha', 'Jl. I Gede Desa 1', '089787678789', 'yoganug', 'yoganug', 'guru', 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_mata_diklat`
--

CREATE TABLE IF NOT EXISTS `kategori_mata_diklat` (
  `kode_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(20) NOT NULL,
  PRIMARY KEY (`kode_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `kategori_mata_diklat`
--

INSERT INTO `kategori_mata_diklat` (`kode_kategori`, `nama_kategori`) VALUES
(1, 'Normatif/Adaptif'),
(2, 'Produktif'),
(3, 'Muatan Lokal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `kode_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kk` int(11) NOT NULL,
  `nama_kelas` varchar(20) NOT NULL,
  `kode_wali_kelas` varchar(5) NOT NULL,
  PRIMARY KEY (`kode_kelas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`kode_kelas`, `kode_kk`, `nama_kelas`, `kode_wali_kelas`) VALUES
(1, 5, 'X RPL I', '66001'),
(3, 6, 'X MM I', '66004'),
(4, 7, 'X TKJ I', '66002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kompetensi_keahlian`
--

CREATE TABLE IF NOT EXISTS `kompetensi_keahlian` (
  `kode_kk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kk` varchar(40) NOT NULL,
  PRIMARY KEY (`kode_kk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `kompetensi_keahlian`
--

INSERT INTO `kompetensi_keahlian` (`kode_kk`, `nama_kk`) VALUES
(2, 'Semua'),
(5, 'Rekayasa Perangkat Lunak');

--
-- Trigger `kompetensi_keahlian`
--
DROP TRIGGER IF EXISTS `hapusDATA`;
DELIMITER //
CREATE TRIGGER `hapusDATA` AFTER DELETE ON `kompetensi_keahlian`
 FOR EACH ROW BEGIN
	DELETE FROM mata_diklat WHERE mata_diklat.kode_kk = OLD.kode_kk;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mata_diklat`
--

CREATE TABLE IF NOT EXISTS `mata_diklat` (
  `kode_mata_diklat` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kategori` int(11) NOT NULL,
  `nama_mata_diklat` varchar(40) NOT NULL,
  `kode_kk` int(11) NOT NULL,
  `kkm` int(11) NOT NULL,
  PRIMARY KEY (`kode_mata_diklat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `mata_diklat`
--

INSERT INTO `mata_diklat` (`kode_mata_diklat`, `kode_kategori`, `nama_mata_diklat`, `kode_kk`, `kkm`) VALUES
(1, 1, 'Matematika', 2, 80),
(2, 1, 'Bahasa Indonesia', 2, 76),
(3, 1, 'Bahasa Inggris', 2, 80),
(5, 2, 'Pemrograman Web Dinamis', 5, 87),
(6, 2, 'Pemrograman Desktop', 5, 87),
(7, 3, 'Bahasa Bali', 2, 80);

--
-- Trigger `mata_diklat`
--
DROP TRIGGER IF EXISTS `hapusSk`;
DELIMITER //
CREATE TRIGGER `hapusSk` BEFORE DELETE ON `mata_diklat`
 FOR EACH ROW DELETE FROM standar_kompetensi WHERE standar_kompetensi.kode_mata_diklat = OLD.kode_mata_diklat
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `nisn` varchar(5) NOT NULL,
  `nip` varchar(5) NOT NULL,
  `kode_sk` int(11) NOT NULL,
  `nilai_angka` int(11) NOT NULL,
  `nilai_huruf` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`nisn`, `nip`, `kode_sk`, `nilai_angka`, `nilai_huruf`) VALUES
('22791', '66004', 5, 80, 'B'),
('22792', '66004', 5, 80, 'B'),
('22793', '66004', 5, 78, 'C'),
('22794', '66004', 5, 90, 'A'),
('22795', '66004', 5, 70, 'C'),
('22791', '66004', 6, 90, 'A'),
('22792', '66004', 6, 90, 'A'),
('22793', '66004', 6, 90, 'A'),
('22794', '66004', 6, 90, 'A'),
('22795', '66004', 6, 98, 'A'),
('22791', '66002', 7, 89, 'B'),
('22792', '66002', 7, 78, 'C'),
('22793', '66002', 7, 90, 'A'),
('22794', '66002', 7, 89, 'B'),
('22795', '66002', 7, 88, 'B'),
('22791', '66002', 8, 80, 'B'),
('22792', '66002', 8, 80, 'B'),
('22793', '66002', 8, 87, 'B'),
('22794', '66002', 8, 86, 'B'),
('22795', '66002', 8, 79, 'C'),
('22791', '66003', 3, 80, 'B'),
('22792', '66003', 3, 90, 'A'),
('22793', '66003', 3, 88, 'B'),
('22794', '66003', 3, 87, 'B'),
('22795', '66003', 3, 90, 'A'),
('22791', '66003', 4, 90, 'A'),
('22792', '66003', 4, 90, 'A'),
('22793', '66003', 4, 87, 'B'),
('22794', '66003', 4, 88, 'B'),
('22795', '66003', 4, 90, 'A'),
('22791', '66006', 9, 89, 'B'),
('22792', '66006', 9, 88, 'B'),
('22793', '66006', 9, 86, 'B'),
('22794', '66006', 9, 90, 'A'),
('22795', '66006', 9, 98, 'A'),
('22791', '66006', 10, 100, 'A'),
('22792', '66006', 10, 90, 'A'),
('22793', '66006', 10, 99, 'A'),
('22794', '66006', 10, 88, 'B'),
('22795', '66006', 10, 87, 'B'),
('22791', '66001', 11, 95, 'A'),
('22792', '66001', 11, 98, 'A'),
('22793', '66001', 11, 98, 'A'),
('22794', '66001', 11, 97, 'A'),
('22795', '66001', 11, 97, 'A'),
('22791', '66001', 12, 98, 'A'),
('22792', '66001', 12, 80, 'B'),
('22793', '66001', 12, 78, 'C'),
('22794', '66001', 12, 90, 'A'),
('22795', '66001', 12, 97, 'A'),
('22791', '66007', 1, 80, 'B'),
('22792', '66007', 1, 80, 'B'),
('22793', '66007', 1, 80, 'B'),
('22794', '66007', 1, 80, 'B'),
('22795', '66007', 1, 80, 'B'),
('22791', '66007', 2, 78, 'C'),
('22792', '66007', 2, 90, 'A'),
('22793', '66007', 2, 99, 'A'),
('22794', '66007', 2, 80, 'B'),
('22795', '66007', 2, 76, 'C'),
('22791', '66007', 13, 90, 'A'),
('22792', '66007', 13, 90, 'A'),
('22793', '66007', 13, 90, 'A'),
('22794', '66007', 13, 90, 'A'),
('22795', '66007', 13, 90, 'A'),
('22791', '66007', 14, 80, 'B'),
('22792', '66007', 14, 76, 'C'),
('22793', '66007', 14, 88, 'B'),
('22794', '66007', 14, 99, 'A'),
('22795', '66007', 14, 78, 'C');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `nisn` varchar(5) NOT NULL,
  `kode_kk` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `alamat_siswa` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `url_foto` text NOT NULL,
  `nama_foto` text NOT NULL,
  `user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `user_type` enum('siswa','','','') NOT NULL DEFAULT 'siswa',
  `kode_wali` int(11) NOT NULL,
  `kode_kelas` int(11) NOT NULL,
  PRIMARY KEY (`nisn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`nisn`, `kode_kk`, `nama_siswa`, `alamat_siswa`, `tgl_lahir`, `url_foto`, `nama_foto`, `user`, `password`, `user_type`, `kode_wali`, `kode_kelas`) VALUES
('22791', 5, 'Januar Kresnamurti', 'Penamparan', '1997-01-13', 'http://localhost/22791/app/uploads/foto_22791.jpg', 'foto_22791.jpg', 'januar', 'januar', 'siswa', 3, 1),
('22792', 5, 'Agnes Rai Madalena', 'Dalung', '1997-03-11', 'http://localhost/22791/app/uploads/foto_22792.jpg', 'foto_22792.jpg', 'agnes', 'agnes', 'siswa', 4, 1),
('22793', 5, 'Ary Aviantara', 'Pemogan', '1996-03-12', 'http://localhost/22791/app/uploads/foto_22793.jpg', 'foto_22793.jpg', 'ary', 'ary', 'siswa', 1, 1),
('22794', 5, 'Abdul Haq', 'Tabanan', '1996-03-21', 'http://localhost/22791/app/uploads/foto_22794.jpg', 'foto_22794.jpg', 'abdul', 'abdul', 'siswa', 5, 1),
('22795', 5, 'Bayu Kresnawan', 'Jl. Surapati Denpasar', '1997-03-04', 'http://localhost/22791/app/uploads/foto_22795.jpg', 'foto_22795.jpg', 'bayukresnawan', 'bayukresnawan', 'siswa', 6, 1);

--
-- Trigger `siswa`
--
DROP TRIGGER IF EXISTS `deleteScores`;
DELIMITER //
CREATE TRIGGER `deleteScores` BEFORE DELETE ON `siswa`
 FOR EACH ROW DELETE FROM nilai WHERE nilai.nisn = OLD.nisn
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `standar_kompetensi`
--

CREATE TABLE IF NOT EXISTS `standar_kompetensi` (
  `kode_sk` int(11) NOT NULL AUTO_INCREMENT,
  `kode_mata_diklat` int(11) NOT NULL,
  `nama_sk` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_sk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `standar_kompetensi`
--

INSERT INTO `standar_kompetensi` (`kode_sk`, `kode_mata_diklat`, `nama_sk`) VALUES
(3, 2, 'Surat'),
(4, 2, 'Puisi'),
(5, 3, 'Gerund'),
(6, 3, 'Conditional Sentence'),
(7, 1, 'Limit & Turunan'),
(8, 1, 'Parabola'),
(9, 6, 'Pemrograman Visual Basic Dasar'),
(10, 6, 'Pemrograman Visual Basic Lanjut'),
(11, 5, 'Pemrograman PHP & MySQL'),
(12, 5, 'Integerasi Server Dengan Program PHP'),
(13, 7, 'Nyurat Lontar'),
(14, 7, 'Mekidung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `kode_user` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `user_type` enum('admin') NOT NULL,
  PRIMARY KEY (`kode_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`kode_user`, `user`, `password`, `user_type`) VALUES
(1, 'dewadg', 'skensajaya', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wali_murid`
--

CREATE TABLE IF NOT EXISTS `wali_murid` (
  `kode_wali` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ayah` varchar(50) NOT NULL,
  `pekerjaan_ayah` varchar(30) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `pekerjaan_ibu` varchar(30) NOT NULL,
  `alamat_wali` text NOT NULL,
  `telp_wali` varchar(20) NOT NULL,
  PRIMARY KEY (`kode_wali`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `wali_murid`
--

INSERT INTO `wali_murid` (`kode_wali`, `nama_ayah`, `pekerjaan_ayah`, `nama_ibu`, `pekerjaan_ibu`, `alamat_wali`, `telp_wali`) VALUES
(1, 'Agung Purbayana', 'Dosen', 'Ferawati', '-', 'Dalung', '085935355888'),
(3, 'Anom Setiawan', 'Wiraswasta', 'Suwendri', '-', 'Penamparan', '087657545654'),
(4, 'Rudita', 'Wiraswasta', 'Darmini', 'Wiraswasta', 'Jl. Merpati', '085935355888'),
(5, 'Muhammad Amin', 'Pedagang', 'Rini', 'Pedagang', 'Tabanan', '0878768787689'),
(6, 'Sugita', 'Dosen', 'Gandawati', '-', 'Jl. Surapati Denpasar', '087676545654');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
