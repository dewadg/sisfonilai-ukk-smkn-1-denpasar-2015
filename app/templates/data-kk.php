<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Kompetensi Keahlian';
	include_once('app-head.php');
	$db = new Database();
	if (!@checkPar($_GET['terms']) && !@checkPar($_GET['keyword'])):
		$sql = 'SELECT * FROM kompetensi_keahlian WHERE nama_kk != "Semua" ORDER BY nama_kk ASC';
	else:
		$sql = 'SELECT * FROM kompetensi_keahlian WHERE nama_kk != "Semua" AND '.$_GET['terms'].' LIKE "%'.$_GET['keyword'].'%" ORDER BY nama_kk ASC';
	endif;
	$stmt	= $db->pdo->prepare($sql);
	$stmt->execute();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form method="get" class="search-bar">
	<input type="hidden" name="page" value="data-kk" />
	<select name="terms">
		<option value="nama_kk">Nama Kompetensi Keahlian</option>
	</select>
	<input type="search" name="keyword" placeholder="Kata kunci" />
	<input type="submit" value="Cari" class="btn" />
	<button type="button" onclick="window.location.href='?page=data-kk'" class="btn">Refresh</button>
	<button type="button" onclick="window.location.href='?page=tambah-kk'" class="btn">Tambah Kompetensi Keahlian</button>
</form>
<table class="data-table">
	<thead>
		<tr>
			<th>No.</th>
			<th>Nama Kompetensi Keahlian</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$num 	= 1;
			while ($data = $stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<td><?= $num; $num++; ?></td>
				<td><?= $data['nama_kk']; ?></td>
				<td>
					<a class="btn" href="?page=edit-kk&kode_kk=<?= $data['kode_kk']; ?>">Edit</a>&nbsp;
					<a class="btn" href="modules/deleteData.php?data_type=kk&kode_kk=<?= $data['kode_kk']; ?>">Hapus</a>
				</td>
			</tr>
		<?php endwhile; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3">Jumlah data: <?= $stmt->rowCount(); ?></td>
		</tr>
	</tfoot>
</table>

<?php include_once('app-foot.php'); messageAlert(); ?>