<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Tambah Mata Diklat';
	include_once('app-head.php');
	$db 	= new Database();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/insertData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="md" />
		<label>Nama Mata Diklat</label>
		<input type="text" name="nama_mata_diklat" maxlength="40" required />
		<label>Jenis</label>
		<select name="kode_kategori">
			<option value="" disabled selected>---</option>
			<?php
				$data	= $db->fetch('kategori_mata_diklat', 'nama_kategori', 'ASC');
				foreach ($data as $row):
			?>
				<option value="<?= $row['kode_kategori']; ?>"><?= $row['nama_kategori']; ?></option>
			<?php endforeach; ?>
		</select>
		<label>Kompetensi Keahlian</label>
		<select name="kode_kk">
			<option value="" disabled selected>---</option>
			<?php
				$data	= $db->fetch('kompetensi_keahlian', 'nama_kk', 'ASC');
				foreach ($data as $row):
			?>
				<option value="<?= $row['kode_kk']; ?>"><?= $row['nama_kk']; ?></option>
			<?php endforeach; ?>
		</select>
		<label>KKM</label>
		<input type="number" name="kkm" maxlength="2" />
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>