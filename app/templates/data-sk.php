<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Standar Kompetensi';
	include_once('app-head.php');
	$db = new Database();
	if (!@checkPar($_GET['terms']) && !@checkPar($_GET['keyword'])):
		$sql = 	'SELECT 
					standar_kompetensi.*,
					mata_diklat.nama_mata_diklat,
					kompetensi_keahlian.nama_kk
				FROM 
					standar_kompetensi
				INNER JOIN mata_diklat ON
					mata_diklat.kode_mata_diklat = standar_kompetensi.kode_mata_diklat
				INNER JOIN kompetensi_keahlian ON
					mata_diklat.kode_kk = kompetensi_keahlian.kode_kk
				ORDER BY nama_sk ASC';
	else:
		$sql = 	'SELECT 
					standar_kompetensi.*,
					mata_diklat.nama_mata_diklat,
					kompetensi_keahlian.nama_kk
				FROM 
					standar_kompetensi
				INNER JOIN mata_diklat ON
					mata_diklat.kode_mata_diklat = standar_kompetensi.kode_mata_diklat
				INNER JOIN kompetensi_keahlian ON
					mata_diklat.kode_kk = kompetensi_keahlian.kode_kk
				WHERE
					'.$_GET['terms'].' LIKE "%'.$_GET['keyword'].'%"
				ORDER BY nama_mata_diklat ASC';
	endif;
	$stmt	= $db->pdo->prepare($sql);
	$stmt->execute();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form method="get" class="search-bar">
	<input type="hidden" name="page" value="data-sk" />
	<select name="terms">
		<option value="standar_kompetensi.nama_sk">Standar Kompetensi</option>
		<option value="mata_diklat.nama_mata_diklat">Mata Diklat</option>
		<option value="kompetensi_keahlian.nama_kk">Kompetensi Keahlian</option>
	</select>
	<input type="search" name="keyword" placeholder="Kata kunci" />
	<input type="submit" value="Cari" class="btn" />
	<button type="button" onclick="window.location.href='?page=data-sk'" class="btn">Refresh</button>
	<button type="button" onclick="window.location.href='?page=tambah-sk'" class="btn">Tambah Standar Kompetensi</button>
</form>
<table class="data-table">
	<thead>
		<tr>
			<th>No.</th>
			<th>Standar Kompetensi</th>
			<th>Mata Diklat</th>
			<th>Kompetensi Keahlian</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$num 	= 1;
			while ($data = $stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<td><?= $num; $num++; ?></td>
				<td><?= $data['nama_sk']; ?></td>
				<td><?= $data['nama_mata_diklat']; ?></td>
				<td><?= $data['nama_kk']; ?></td>
				<td>
					<a class="btn" href="?page=edit-sk&kode_sk=<?= $data['kode_sk']; ?>">Edit</a>&nbsp;
					<a class="btn" href="modules/deleteData.php?data_type=sk&kode_sk=<?= $data['kode_sk']; ?>">Hapus</a>
				</td>
			</tr>
		<?php endwhile; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">Jumlah data: <?= $stmt->rowCount(); ?></td>
		</tr>
	</tfoot>
</table>

<?php include_once('app-foot.php'); messageAlert(); ?>