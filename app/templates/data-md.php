<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Mata Diklat';
	include_once('app-head.php');
	$db = new Database();
	if (!@checkPar($_GET['terms']) && !@checkPar($_GET['keyword'])):
		$sql = 	'SELECT 
					mata_diklat.*,
					kompetensi_keahlian.nama_kk
				FROM 
					mata_diklat 
				INNER JOIN kompetensi_keahlian ON
					kompetensi_keahlian.kode_kk = mata_diklat.kode_kk
				ORDER BY nama_mata_diklat ASC';
	else:
		$sql = 	'SELECT 
					mata_diklat.*,
					kompetensi_keahlian.nama_kk
				FROM 
					mata_diklat
				INNER JOIN kompetensi_keahlian ON
					kompetensi_keahlian.kode_kk = mata_diklat.kode_kk
				WHERE 
					'.$_GET['terms'].' LIKE "%'.$_GET['keyword'].'%" 
				ORDER BY nama_mata_diklat ASC';
	endif;
	$stmt	= $db->pdo->prepare($sql);
	$stmt->execute();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form method="get" class="search-bar">
	<input type="hidden" name="page" value="data-md" />
	<select name="terms">
		<option value="nama_mata_diklat">Nama Mata Diklat</option>
	</select>
	<input type="search" name="keyword" placeholder="Kata kunci" />
	<input type="submit" value="Cari" class="btn" />
	<button type="button" onclick="window.location.href='?page=data-md'" class="btn">Refresh</button>
	<button type="button" onclick="window.location.href='?page=tambah-md'" class="btn">Tambah Mata Diklat</button>
</form>
<table class="data-table">
	<thead>
		<tr>
			<th>No.</th>
			<th>Nama Mata Diklat</th>
			<th>Kompetensi Keahlian</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$num 	= 1;
			while ($data = $stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<td><?= $num; $num++; ?></td>
				<td><?= $data['nama_mata_diklat']; ?></td>
				<td><?= $data['nama_kk']; ?></td>
				<td>
					<a class="btn" href="?page=edit-md&kode_mata_diklat=<?= $data['kode_mata_diklat']; ?>">Edit</a>&nbsp;
					<a class="btn" href="modules/deleteData.php?data_type=md&kode_mata_diklat=<?= $data['kode_mata_diklat']; ?>">Hapus</a>
				</td>
			</tr>
		<?php endwhile; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4">Jumlah data: <?= $stmt->rowCount(); ?></td>
		</tr>
	</tfoot>
</table>

<?php include_once('app-foot.php'); messageAlert(); ?>