<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Tambah Kelas';
	include_once('app-head.php');
	$db 	= new Database();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/insertData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="kelas" />
		<label>Nama Kelas</label>
		<input type="text" name="nama_kelas" maxlength="20" required />
		<label>Kompetensi Keahlian</label>
		<select name="kode_kk">
			<option value="" disabled selected>---</option>
			<?php
				$stmt	= $db->pdo->prepare('SELECT * FROM kompetensi_keahlian WHERE nama_kk != "Semua"');
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)):
			?>
				<option value="<?= $row['kode_kk']; ?>"><?= $row['nama_kk']; ?></option>
			<?php endwhile; ?>
		</select>
		<label>Wali Kelas</label>
		<select name="kode_wali_kelas">
			<option value="" disabled selected>---</option>
			<?php
				$gr_data	= $db->fetch('guru', 'nama_guru', 'ASC');
				foreach ($gr_data as $row):
			?>
				<option value="<?= $row['nip']; ?>"><?= $row['nama_guru']; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>