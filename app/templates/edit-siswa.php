<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	if (!@checkPar($_GET['nisn'])): switchPage('data-siswa'); endif;
	$page	= 'Edit Siswa';
	include_once('app-head.php');
	$db 	= new Database();
	$stmt	= $db->pdo->prepare('CALL selectSiswa("'.$_GET['nisn'].'")');
	$stmt->execute();
	$data 	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/updateData.php" method="post" enctype="multipart/form-data">
	<div class="half">
		<input type="hidden" name="data_type" value="siswa" />
		<input type="hidden" name="nisn" value="<?= $_GET['nisn']; ?>" required />
		<label>NISN</label>
		<input type="text" value="<?= $_GET['nisn']; ?>" disabled />
		<label>Nama Siswa</label>
		<input type="text" name="nama_siswa" maxlength="50" value="<?= $data['nama_siswa']; ?>" required />
		<label>Alamat</label>
		<input type="text" name="alamat_siswa" value="<?= $data['alamat_siswa']; ?>" required />
		<label>Tanggal Lahir</label>
		<input type="date" name="tgl_lahir" value="<?= $data['tgl_lahir']; ?>" required />
		<label>Foto</label>
		<img src="<?= $data['url_foto']; ?>" style="width: auto; height: 180px; display: block; margin: 0 auto 20px auto;" />
		<input type="file" name="foto" />
	</div>
	<div class="half">
		<label>Kompetensi Keahlian</label>
		<select name="kode_kk">
			<option value="" disabled selected>---</option>
			<?php
				$stmt 	= $db->pdo->prepare('SELECT * FROM kompetensi_keahlian WHERE nama_kk != "Semua"');
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)):
			?>
				<option value="<?= $row['kode_kk']; ?>" <?= matchVals($row['kode_kk'], $data['kode_kk'], 'selected'); ?>><?= $row['nama_kk']; ?>
			<?php endwhile; ?>
 		</select>
		<label>Kelas</label>
		<select name="kode_kelas">
			<option value="" disabled selected>---</option>
			<?php
				$md_data	= $db->fetch('kelas', 'nama_kelas', 'ASC');
				foreach ($md_data as $row):
			?>
				<option value="<?= $row['kode_kelas']; ?>" <?= matchVals($row['kode_kelas'], $data['kode_kelas'], 'selected'); ?>><?= $row['nama_kelas']; ?>
			<?php endforeach; ?>
		</select> 
		<label>Wali Siswa</label>
		<select name="kode_wali">
			<option value="" disabled selected>---</option>
			<?php
				$md_data	= $db->fetch('wali_murid', 'nama_ayah', 'ASC');
				foreach ($md_data as $row):
			?>
				<option value="<?= $row['kode_wali']; ?>" <?= matchVals($row['kode_wali'], $data['kode_wali'], 'selected'); ?>><?= $row['nama_ayah'].' - '.$row['nama_ibu']; ?>
			<?php endforeach; ?>
		</select> 
		<label>Nama Pengguna</label>
		<input type="text" name="user" maxlength="20" value="<?= $data['user']; ?>" required />
		<label>Kata Sandi</label>
		<input type="text" name="password" maxlength="20" value="<?= $data['password']; ?>" required />
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>