<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Tambah Standar Kompetensi';
	include_once('app-head.php');
	$db 	= new Database();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/insertData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="sk" />
		<label>Nama Standar Kompetensi</label>
		<input type="text" name="nama_sk" maxlength="40" required />
		<label>Mata Diklat</label>
		<select name="kode_mata_diklat">
			<?php
				$md_data	= $db->fetch('mata_diklat', 'nama_mata_diklat', 'ASC');
				foreach ($md_data as $row):
			?>
				<option value="<?= $row['kode_mata_diklat']; ?>"><?= $row['nama_mata_diklat']; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>