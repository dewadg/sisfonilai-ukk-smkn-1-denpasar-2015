<?php
	if (!isLogged()): switchPage('index&message=1'); endif;
	$page	= 'Cetak Rapot';
	include_once('app-head.php');
	$db = new Database();
	
	if (!@checkPar($_GET['kode_kk'])):	
		include_once('rapot-pilih-kk.php');
	elseif (!@checkPar($_GET['kode_kelas']) && checkPar($_GET['kode_kk'])):
		include_once('rapot-pilih-kelas.php');
	elseif (!@checkPar($_GET['nisn']) && checkPar($_GET['kode_kelas'])):
		include_once('rapot-pilih-siswa.php');
	elseif (@checkPar($_GET['nisn'])):
			include_once('rapot-rapot.php');
	endif;
	
	include_once('app-foot.php'); messageAlert(); ?>