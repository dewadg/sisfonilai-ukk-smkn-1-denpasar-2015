<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	if (!checkPar($_GET['kode_mata_diklat'])): switchPage('data-md'); endif;
	$page	= 'Edit Mata Diklat';
	include_once('app-head.php');
	$db 	= new Database();
	$stmt	= $db->pdo->prepare('SELECT * FROM mata_diklat WHERE kode_mata_diklat = "'.$_GET['kode_mata_diklat'].'"');
	$stmt->execute();
	$data 	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/updateData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="md" />
		<input type="hidden" name="kode_mata_diklat" value="<?= $_GET['kode_mata_diklat']; ?>" />
		<label>Nama Mata Diklat</label>
		<input type="text" name="nama_mata_diklat" maxlength="40" value="<?= $data['nama_mata_diklat']; ?>" required />
		<label>Jenis</label>
		<select name="kode_kategori">
			<?php
				$kk_data = $db->fetch('kategori_mata_diklat', 'nama_kategori', 'ASC');
				foreach ($kk_data as $row):
			?>
				<option value="<?= $row['kode_kategori']; ?>" <?= matchVals($row['kode_kategori'], $data['kode_kategori'], 'selected'); ?>><?= $row['nama_kategori']; ?></option>
			<?php endforeach;?>
		</select>
		<label>Kompetensi Keahlian</label>
		<select name="kode_kk">
			<?php
				$kk_data	= $db->fetch('kompetensi_keahlian', 'nama_kk', 'ASC');
				foreach ($kk_data as $row):
			?>
				<option value="<?= $row['kode_kk']; ?>" <?= matchVals($row['kode_kk'], $data['kode_kk'], 'selected'); ?>><?= $row['nama_kk']; ?></option>
			<?php endforeach; ?>
		</select>
		<label>KKM</label>
		<input type="text" name="kkm" maxlength="2" value="<?= $data['kkm']; ?>" />
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>