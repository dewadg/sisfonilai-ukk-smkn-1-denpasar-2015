<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	if (!@checkPar($_GET['kode_wali'])): switchPage('data-wali'); endif;
	$page	= 'Edit Wali';
	include_once('app-head.php');
	$db 	= new Database();
	$stmt	= $db->pdo->prepare('SELECT * FROM wali_murid WHERE kode_wali = "'.$_GET['kode_wali'].'"');
	$stmt->execute();
	$data 	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/updateData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="wali" />
		<input type="hidden" name="kode_wali" value="<?= $_GET['kode_wali']; ?>" />
		<label>Nama Ayah</label>
		<input type="text" name="nama_ayah" maxlength="50" value="<?= $data['nama_ayah']; ?>" required />
		<label>Pekerjaan Ayah</label>
		<input type="text" name="pekerjaan_ayah" maxlength="30" value="<?= $data['pekerjaan_ayah']; ?>" required />
		<label>Nama Ibu</label>
		<input type="text" name="nama_ibu" maxlength="50" value="<?= $data['nama_ibu']; ?>" required />
		<label>Pekerjaan Ibu</label>
		<input type="text" name="pekerjaan_ibu" maxlength="30" value="<?= $data['pekerjaan_ibu']; ?>" required />
	</div>
	<div class="half">
		<label>Alamat</label>
		<input type="text" name="alamat_wali" value="<?= $data['alamat_wali']; ?>" required />
		<label>Telpon</label>
		<input type="text" name="telp_wali" maxlength="20" value="<?= $data['telp_wali']; ?>" required />
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>