<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Tambah Wali';
	include_once('app-head.php');
	$db 	= new Database();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/insertData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="wali" />
		<label>Nama Ayah</label>
		<input type="text" name="nama_ayah" maxlength="50" required />
		<label>Pekerjaan Ayah</label>
		<input type="text" name="pekerjaan_ayah" maxlength="30" required />
		<label>Nama Ibu</label>
		<input type="text" name="nama_ibu" maxlength="50" required />
		<label>Pekerjaan Ibu</label>
		<input type="text" name="pekerjaan_ibu" maxlength="30" required />
	</div>
	<div class="half">
		<label>Alamat</label>
		<input type="text" name="alamat_wali" required />
		<label>Telpon</label>
		<input type="text" name="telp_wali" maxlength="20" required />
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>