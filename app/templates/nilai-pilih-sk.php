<h2 class="page-title">Pilih Standar Kompetensi</h2>
<div class="half">
	<form method="get">
		<input type="hidden" name="page" value="nilai" />
		<input type="hidden" name="kode_kk" value="<?= $_GET['kode_kk']; ?>" />
		<input type="hidden" name="kode_kelas" value="<?= $_GET['kode_kelas']; ?>" />
 		<select name="kode_sk">
			<?php	
				$sql 	=	'SELECT
								*
							FROM
								standar_kompetensi
							INNER JOIN mata_diklat ON
								mata_diklat.kode_mata_diklat = standar_kompetensi.kode_mata_diklat
							INNER JOIN guru ON
								guru.kode_mata_diklat = mata_diklat.kode_mata_diklat
							WHERE
								guru.nip = "'.$_SESSION['nip'].'"
							';
				$stmt 	= $db->pdo->prepare($sql);
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)):
			?>
				<option value="<?= $row['kode_sk']; ?>"><?= $row['nama_sk']; ?></option>
			<?php endwhile; ?>
		</select>
		<input type="submit" value="Berikutnya" class="btn" />
	</form>
</div>