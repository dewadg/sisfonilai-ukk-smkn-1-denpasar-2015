<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Tambah Siswa';
	include_once('app-head.php');
	$db 	= new Database();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/insertData.php" method="post" enctype="multipart/form-data">
	<div class="half">
		<input type="hidden" name="data_type" value="siswa" required />
		<label>NISN</label>
		<input type="text" name="nisn" maxlength="5" required />
		<label>Nama Siswa</label>
		<input type="text" name="nama_siswa" maxlength="50" required  />
		<label>Alamat</label>
		<input type="text" name="alamat_siswa" required />
		<label>Tanggal Lahir</label>
		<input type="date" name="tgl_lahir" required />
		<label>Foto</label>
		<input type="file" name="foto" required />
	</div>
	<div class="half">
		<label>Kompetensi Keahlian</label>
		<select name="kode_kk">
			<option value="" disabled selected>---</option>
			<?php
				$stmt 	= $db->pdo->prepare('SELECT * FROM kompetensi_keahlian WHERE nama_kk != "Semua"');
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)):
			?>
				<option value="<?= $row['kode_kk']; ?>"><?= $row['nama_kk']; ?>
			<?php endwhile; ?>
 		</select>
		<label>Kelas</label>
		<select name="kode_kelas">
			<option value="" disabled selected>---</option>
			<?php
				$md_data	= $db->fetch('kelas', 'nama_kelas', 'ASC');
				foreach ($md_data as $row):
			?>
				<option value="<?= $row['kode_kelas']; ?>"><?= $row['nama_kelas']; ?>
			<?php endforeach; ?>
		</select> 
		<label>Wali Siswa</label>
		<select name="kode_wali">
			<option value="" disabled selected>---</option>
			<?php
				$md_data	= $db->fetch('wali_murid', 'nama_ayah', 'ASC');
				foreach ($md_data as $row):
			?>
				<option value="<?= $row['kode_wali']; ?>"><?= $row['nama_ayah'].' - '.$row['nama_ibu']; ?>
			<?php endforeach; ?>
		</select> 
		<label>Nama Pengguna</label>
		<input type="text" name="user" maxlength="20" required />
		<label>Kata Sandi</label>
		<input type="text" name="password" maxlength="20" required />
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>