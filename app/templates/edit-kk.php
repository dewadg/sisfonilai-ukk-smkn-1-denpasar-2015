<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	if (!@checkPar($_GET['kode_kk'])): switchPage('data-kk'); endif;
	$page	= 'Edit Kompetensi Keahlian';
	include_once('app-head.php');
	$db 	= new Database();
	$stmt	= $db->pdo->prepare('SELECT * FROM kompetensi_keahlian WHERE kode_kk = "'.$_GET['kode_kk'].'"');
	$stmt->execute();
	$data 	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/updateData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="kk" />
		<input type="hidden" name="kode_kk" value="<?= $_GET['kode_kk']; ?>" />
		<label>Nama Kompetensi Keahlian</label>
		<input type="text" name="nama_kk" maxlength="40" value="<?= $data['nama_kk']; ?>" required />
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>