<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	if (!@checkPar($_GET['kode_kelas'])): switchPage('data-kelas'); endif;
	$page	= 'Edit Kelas';
	include_once('app-head.php');
	$db 	= new Database();
	$stmt 	= $db->pdo->prepare('SELECT * FROM kelas WHERE kode_kelas = "'.$_GET['kode_kelas'].'"');
	$stmt->execute();
	$data 	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/updateData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="kelas" />
		<input type="hidden" name="kode_kelas" value="<?= $_GET['kode_kelas']; ?>" />
		<label>Nama Kelas</label>
		<input type="text" name="nama_kelas" maxlength="20" value="<?= $data['nama_kelas']; ?>" required />
		<label>Kompetensi Keahlian</label>
		<select name="kode_kk">
			<option value="" disabled selected>---</option>
			<?php
				$stmt	= $db->pdo->prepare('SELECT * FROM kompetensi_keahlian WHERE nama_kk != "Semua"');
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)):
			?>
				<option value="<?= $row['kode_kk']; ?>" <?= matchVals($row['kode_kk'], $data['kode_kk'], 'selected'); ?>><?= $row['nama_kk']; ?></option>
			<?php endwhile; ?>
		</select>
		<label>Wali Kelas</label>
		<select name="kode_wali_kelas">
			<option value="" disabled selected>---</option>
			<?php
				$gr_data	= $db->fetch('guru', 'nama_guru', 'ASC');
				foreach ($gr_data as $row):
			?>
				<option value="<?= $row['nip']; ?>" <?= matchVals($row['kode_kk'], $data['kode_kk'], 'selected'); ?>><?= $row['nama_guru']; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>