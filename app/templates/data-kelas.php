<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Kelas';
	include_once('app-head.php');
	$db = new Database();
	if (!@checkPar($_GET['terms']) && !@checkPar($_GET['keyword'])):
		$sql = 	'SELECT 
					kelas.*,
					kompetensi_keahlian.nama_kk
				FROM
					kelas
				INNER JOIN kompetensi_keahlian ON
					kompetensi_keahlian.kode_kk = kelas.kode_kk
				ORDER BY kelas.nama_kelas ASC';
	else:
		$sql = 	'SELECT 
					kelas.*,
					kompetensi_keahlian.nama_kk
				FROM
					kelas
				INNER JOIN kompetensi_keahlian ON
					kompetensi_keahlian.kode_kk = kelas.kode_kk
				WHERE 
					'.$_GET['terms'].' LIKE "%'.$_GET['keyword'].'%" 
				ORDER BY kelas.nama_kelas ASC';
	endif;
	$stmt	= $db->pdo->prepare($sql);
	$stmt->execute();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form method="get" class="search-bar">
	<input type="hidden" name="page" value="data-guru" />
	<select name="terms">
		<option value="kelas.nama_kelas">Nama Kelas</option>
		<option value="kompetensi_keahlian.nama_kk">Kompetensi Keahlian</option>
	</select>
	<input type="search" name="keyword" placeholder="Kata kunci" />
	<input type="submit" value="Cari" class="btn" />
	<button type="button" onclick="window.location.href='?page=data-kelas'" class="btn">Refresh</button>
	<button type="button" onclick="window.location.href='?page=tambah-kelas'" class="btn">Tambah Kelas</button>
</form>
<table class="data-table">
	<thead>
		<tr>
			<th>No.</th>
			<th>Kelas</th>
			<th>Kompetensi Keahlian</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$num = 1;
			while ($data = $stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<td><?= $num; $num++; ?></td>
				<td><?= $data['nama_kelas']; ?></td>
				<td><?= $data['nama_kk']; ?></td>
				<td>
					<a class="btn" href="?page=edit-kelas&kode_kelas=<?= $data['kode_kelas']; ?>">Edit</a>&nbsp;
					<a class="btn" href="modules/deleteData.php?data_type=kelas&kode_kelas=<?= $data['kode_kelas']; ?>">Hapus</a>
				</td>
			</tr>
		<?php endwhile; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4">Jumlah data: <?= $stmt->rowCount(); ?></td>
		</tr>
	</tfoot>
</table>

<?php include_once('app-foot.php'); messageAlert(); ?>