<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	if (!@checkPar($_GET['nip'])): switchPage('data-guru'); endif;
	$page	= 'Edit Guru';
	include_once('app-head.php');
	$db 	= new Database();
	$stmt	= $db->pdo->prepare('SELECT * FROM guru WHERE nip = "'.$_GET['nip'].'"');
	$stmt->execute();
	$data 	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/updateData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="guru" />
		<label>Nomor Induk Pengajar</label>
		<input type="text" value="<?= $data['nip']; ?>" disabled />
		<input type="hidden" name="nip" value="<?= $data['nip']; ?>" />
		<label>Nama Guru</label>
		<input type="text" name="nama_guru" maxlength="50" value="<?= $data['nama_guru']; ?>" required />
		<label>Alamat</label>
		<input type="text" name="alamat_guru" value="<?= $data['alamat_guru']; ?>"  required />
		<label>Telpon</label>
		<input type="text" name="telp_guru" maxlength="20" value="<?= $data['telp_guru']; ?>"  required />
	</div>
	<div class="half">
		<label>Kompetensi Keahlian</label>
		<select name="kode_kk">
			<option value="" disabled selected>---</option>
			<?php
				$kk_data	= $db->fetch('kompetensi_keahlian', 'nama_kk', 'ASC');
				foreach ($kk_data as $row):
			?>
				<option value="<?= $row['kode_kk']; ?>" <?= matchVals($row['kode_kk'], $data['kode_kk'], 'selected'); ?>><?= $row['nama_kk']; ?>
			<?php endforeach; ?>
 		</select>
		<label>Mata Diklat</label>
		<select name="kode_mata_diklat">
			<option value="" disabled selected>---</option>
			<?php
				$md_data	= $db->fetch('mata_diklat', 'nama_mata_diklat', 'ASC');
				foreach ($md_data as $row):
			?>
				<option value="<?= $row['kode_mata_diklat']; ?>" <?= matchVals($row['kode_mata_diklat'], $data['kode_mata_diklat'], 'selected'); ?>><?= $row['nama_mata_diklat']; ?>
			<?php endforeach; ?>
		</select> 
		<label>Nama Pengguna</label>
		<input type="text" name="user" maxlength="20" value="<?= $data['user']; ?>"   required />
		<label>Kata Sandi</label>
		<input type="text" name="password" maxlength="20" value="<?= $data['password']; ?>"   required />
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>