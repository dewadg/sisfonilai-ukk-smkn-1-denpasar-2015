<h2 class="page-title">Pilih Kelas</h2>
<div class="half">
	<form method="get">
		<input type="hidden" name="page" value="nilai" />
		<input type="hidden" name="kode_kk" value="<?= $_GET['kode_kk']; ?>" />
 		<select name="kode_kelas">
			<?php	
				$stmt = $db->pdo->prepare('SELECT * FROM kelas WHERE kode_kk = "'.$_GET['kode_kk'].'"');
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)):
			?>
				<option value="<?= $row['kode_kelas']; ?>"><?= $row['nama_kelas']; ?></option>
			<?php endwhile; ?>
		</select>
		<input type="submit" value="Berikutnya" class="btn" />
	</form>
</div>