<h2 class="page-title">Pilih Kompetensi Keahlian</h2>
<div class="half">
	<form method="get">
		<input type="hidden" name="page" value="rapot" />
		<select name="kode_kk">
			<?php	
				$stmt	= $db->pdo->prepare('SELECT * FROM kompetensi_keahlian WHERE nama_kk != "Semua" ORDER BY nama_kk ASC');
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)):
			?>
				<option value="<?= $row['kode_kk']; ?>"><?= $row['nama_kk']; ?></option>
			<?php endwhile; ?>
		</select>
		<input type="submit" value="Berikutnya" class="btn" />
	</form>
</div>