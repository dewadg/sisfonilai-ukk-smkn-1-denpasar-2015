<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Siswa';
	include_once('app-head.php');
	$db = new Database();
	if (!@checkPar($_GET['terms']) && !@checkPar($_GET['keyword'])):
		$sql = 	'SELECT 
					siswa.*,
					kompetensi_keahlian.nama_kk,
					kelas.nama_kelas
				FROM
					siswa
				INNER JOIN kompetensi_keahlian ON
					kompetensi_keahlian.kode_kk = siswa.kode_kk
				INNER JOIN kelas ON
					kelas.kode_kelas = siswa.kode_kelas
				ORDER BY kelas.nama_kelas ASC';
	else:
		$sql = 	'SELECT 
					siswa.*,
					kompetensi_keahlian.nama_kk,
					kelas.nama_kelas
				FROM
					siswa
				INNER JOIN kompetensi_keahlian ON
					kompetensi_keahlian.kode_kk = siswa.kode_kk
				INNER JOIN kelas ON
					kelas.kode_kelas = siswa.kode_kelas
				WHERE 
					'.$_GET['terms'].' LIKE "%'.$_GET['keyword'].'%" 
				ORDER BY kelas.nama_kelas ASC';
	endif;
	$stmt	= $db->pdo->prepare($sql);
	$stmt->execute();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form method="get" class="search-bar">
	<input type="hidden" name="page" value="data-guru" />
	<select name="terms">
		<option value="guru.nama_guru">Nama Siswa</option>
		<option value="kompetensi_keahlian.nama_kk">Kompetensi Keahlian</option>
	</select>
	<input type="search" name="keyword" placeholder="Kata kunci" />
	<input type="submit" value="Cari" class="btn" />
	<button type="button" onclick="window.location.href='?page=data-siswa'" class="btn">Refresh</button>
	<button type="button" onclick="window.location.href='?page=tambah-siswa'" class="btn">Tambah Siswa</button>
</form>
<table class="data-table">
	<thead>
		<tr>
			<th>NISN</th>
			<th>Nama Siswa</th>
			<th>Kelas</th>
			<th>Kompetensi Keahlian</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		<?php
			while ($data = $stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<td><?= $data['nisn']; ?></td>
				<td><?= $data['nama_siswa']; ?></td>
				<td><?= $data['nama_kelas']; ?></td>
				<td><?= $data['nama_kk']; ?></td>
				<td>
					<a class="btn" href="?page=edit-siswa&nisn=<?= $data['nisn']; ?>">Edit</a>&nbsp;
					<a class="btn" href="modules/deleteData.php?data_type=siswa&nisn=<?= $data['nisn']; ?>">Hapus</a>
				</td>
			</tr>
		<?php endwhile; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">Jumlah data: <?= $stmt->rowCount(); ?></td>
		</tr>
	</tfoot>
</table>

<?php include_once('app-foot.php'); messageAlert(); ?>