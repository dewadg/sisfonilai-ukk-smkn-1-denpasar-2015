<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	if (!@checkPar($_GET['kode_sk'])): switchPage('data-sk'); endif;
	$page	= 'Tambah Standar Kompetensi';
	include_once('app-head.php');
	$db 	= new Database();
	$stmt	= $db->pdo->prepare('SELECT * FROM standar_kompetensi WHERE kode_sk = "'.$_GET['kode_sk'].'"');
	$stmt->execute();
	$data 	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

<h2 class="page-title"><?= $page; ?></h2>
<form action="modules/updateData.php" method="post">
	<div class="half">
		<input type="hidden" name="data_type" value="sk" />
		<input type="hidden" name="kode_sk" value="<?= $_GET['kode_sk']; ?>" />
		<label>Nama Standar Kompetensi</label>
		<input type="text" name="nama_sk" maxlength="40" value="<?= $data['nama_sk']; ?>" required />
		<label>Mata Diklat</label>
		<select name="kode_mata_diklat">
			<?php
				$md_data	= $db->fetch('mata_diklat', 'nama_mata_diklat', 'ASC');
				foreach ($md_data as $row):
			?>
				<option value="<?= $row['kode_mata_diklat']; ?>" <?= matchVals($row['kode_mata_diklat'], $data['kode_mata_diklat'], 'selected'); ?>><?= $row['nama_mata_diklat']; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="clr"></div>
	<div class="full">
		<input type="submit" class="btn" value="Simpan" />
	</div>
</form>

<?php include_once('app-foot.php'); messageAlert(); ?>