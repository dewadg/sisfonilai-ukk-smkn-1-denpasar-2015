<?php
	if (@isLogged()): switchPage('main'); endif;
	$page	= 'Login ke Aplikasi';
?>
<html>
<head>
<title><?= $page; ?></title>
<link rel="stylesheet" href="<?= baseUrl('templates/style.css'); ?>" type="text/css" />
</head>

<body>
	<div class="login">
		<form action="modules/doLogin.php" method="post">	
			<input type="text" name="user" placeholder="Nama pengguna" />
			<input type="password" name="password" placeholder="Kata sandi" />
			<select name="user_type">	
				<option value="user">Admin</option>
				<option value="guru">Guru</option>
				<option value="siswa">Siswa</option>
			</select>
			<input type="submit" value="Login" name="login" class="btn" />
		</form>
	</div>
</body>
</html>
<?php @messageAlert(); ?>