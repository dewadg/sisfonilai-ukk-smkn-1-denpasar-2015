<?php
	if (!isLogged()): switchPage('index&message=1'); endif;
	$page	= 'Halaman Tidak Ditemukan';
	include_once('app-head.php');
?>

<div class="main">
	<h1>Halaman Tidak Ditemukan</h1>
	<p>Halaman yang anda cari tidak ditemukan. Halaman mungkin sudah dihapus/dipindah secara permanen</p>
</div>

<?php include_once('app-foot.php'); messageAlert(); ?>