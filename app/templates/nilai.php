<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('guru')): switchPage('main&message=3'); endif; endif;
	$page	= 'Input Nilai';
	include_once('app-head.php');
	$db = new Database();
	
	if (!@checkPar($_GET['kode_kk'])):	
		include_once('nilai-pilih-kk.php');
	elseif (!@checkPar($_GET['kode_kelas']) && checkPar($_GET['kode_kk'])):
		include_once('nilai-pilih-kelas.php');
	elseif (!@checkPar($_GET['kode_sk']) && checkPar($_GET['kode_kelas'])):
		include_once('nilai-pilih-sk.php');
	elseif (checkPar($_GET['kode_kelas'])):
		include_once('nilai-input-nilai.php');
	endif;
	
	include_once('app-foot.php'); messageAlert(); ?>