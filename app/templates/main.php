<?php
	if (!isLogged()): switchPage('index&message=1'); endif;
	$page	= 'Selamat Datang';
	include_once('app-head.php');
?>

<div class="main">
	<h1>SMK PGRI 3 Badung</h1>
	<p>Alamat: Jl. W. Gebyag No. 5 Dalung</p>
	<p>Telpon: 0361 7830611</p>
	<p>Website: www.smkpgri3badung.sch.id</p>
	<p>Email: info@smkpgri3badung.sch.id</p>
	<br/>
	<p class="author">Copyright &copy; 2015 DEWADG.net</p>
</div>

<?php include_once('app-foot.php'); ?>