<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Wali';
	include_once('app-head.php');
	$db = new Database();
	if (!@checkPar($_GET['terms']) && !@checkPar($_GET['keyword'])):
		$sql = 	'SELECT 
					wali_murid.*,
					siswa.nama_siswa
				FROM
					wali_murid
				LEFT JOIN siswa ON
					wali_murid.kode_wali = siswa.kode_wali
				ORDER BY wali_murid.nama_ayah ASC';
	else:
		$sql = 	'SELECT 
					wali_murid.*,
					siswa.nama_siswa
				FROM
					wali_murid
				LEFT JOIN siswa ON
					wali_murid.kode_wali = siswa.kode_wali
				WHERE 
					'.$_GET['terms'].' LIKE "%'.$_GET['keyword'].'%" 
				ORDER BY wali_murid.nama_ayah ASC';
	endif;
	$stmt	= $db->pdo->prepare($sql);
	$stmt->execute();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form method="get" class="search-bar">
	<input type="hidden" name="page" value="data-wali" />
	<select name="terms">
		<option value="wali_murid.nama_ayah">Nama Ayah</option>
		<option value="wali_murid.nama_ibu">Nama Ibu</option>
		<option value="siswa.nama_siswa">Nama Siswa</option>
	</select>
	<input type="search" name="keyword" placeholder="Kata kunci" />
	<input type="submit" value="Cari" class="btn" />
	<button type="button" onclick="window.location.href='?page=data-wali'" class="btn">Refresh</button>
	<button type="button" onclick="window.location.href='?page=tambah-wali'" class="btn">Tambah Wali</button>
</form>
<table class="data-table">
	<thead>
		<tr>
			<th>No.</th>
			<th>Nama Ayah</th>
			<th>Nama Ibu</th>
			<th>Nama Siswa</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$num = 1;
			while ($data = $stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<td><?= $num; $num++ ?></td>
				<td><?= $data['nama_ayah']; ?></td>
				<td><?= $data['nama_ibu']; ?></td>
				<td><?= $data['nama_siswa']; ?></td>
				<td>
					<a class="btn" href="?page=edit-wali&kode_wali=<?= $data['kode_wali']; ?>">Edit</a>&nbsp;
					<a class="btn" href="modules/deleteData.php?data_type=wali&kode_wali=<?= $data['kode_wali']; ?>">Hapus</a>
				</td>
			</tr>
		<?php endwhile; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">Jumlah data: <?= $stmt->rowCount(); ?></td>
		</tr>
	</tfoot>
</table>

<?php include_once('app-foot.php'); messageAlert(); ?>