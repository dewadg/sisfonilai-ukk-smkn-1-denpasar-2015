<?php
	if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
	$page	= 'Guru';
	include_once('app-head.php');
	$db = new Database();
	if (!@checkPar($_GET['terms']) && !@checkPar($_GET['keyword'])):
		$sql = 	'SELECT 
					guru.*,
					kompetensi_keahlian.nama_kk
				FROM
					guru
				INNER JOIN kompetensi_keahlian ON
					kompetensi_keahlian.kode_kk = guru.kode_kk
				ORDER BY guru.nama_guru ASC';
	else:
		$sql = 	'SELECT 
					guru.*,
					kompetensi_keahlian.nama_kk
				FROM
					guru
				INNER JOIN kompetensi_keahlian ON
					kompetensi_keahlian.kode_kk = guru.kode_kk
				WHERE 
					'.$_GET['terms'].' LIKE "%'.$_GET['keyword'].'%" 
				ORDER BY guru.nama_guru ASC';
	endif;
	$stmt	= $db->pdo->prepare($sql);
	$stmt->execute();
?>

<h2 class="page-title"><?= $page; ?></h2>
<form method="get" class="search-bar">
	<input type="hidden" name="page" value="data-guru" />
	<select name="terms">
		<option value="guru.nama_guru">Nama Guru</option>
		<option value="kompetensi_keahlian.nama_kk">Kompetensi Keahlian</option>
	</select>
	<input type="search" name="keyword" placeholder="Kata kunci" />
	<input type="submit" value="Cari" class="btn" />
	<button type="button" onclick="window.location.href='?page=data-guru'" class="btn">Refresh</button>
	<button type="button" onclick="window.location.href='?page=tambah-guru'" class="btn">Tambah Guru</button>
</form>
<table class="data-table">
	<thead>
		<tr>
			<th>NIP</th>
			<th>Nama Guru</th>
			<th>Kompetensi Keahlian</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		<?php
			while ($data = $stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<td><?= $data['nip']; ?></td>
				<td><?= $data['nama_guru']; ?></td>
				<td><?= $data['nama_kk']; ?></td>
				<td>
					<a class="btn" href="?page=edit-guru&nip=<?= $data['nip']; ?>">Edit</a>&nbsp;
					<a class="btn" href="modules/deleteData.php?data_type=guru&nip=<?= $data['nip']; ?>">Hapus</a>
				</td>
			</tr>
		<?php endwhile; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4">Jumlah data: <?= $stmt->rowCount(); ?></td>
		</tr>
	</tfoot>
</table>

<?php include_once('app-foot.php'); messageAlert(); ?>