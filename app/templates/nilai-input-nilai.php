<?php 
	$k_stmt 	= $db->pdo->prepare('SELECT * FROM kompetensi_keahlian WHERE kode_kk = "'.$_GET['kode_kk'].'"');
	$k_stmt->execute();
	$k_data 	= $k_stmt->fetch(PDO::FETCH_OBJ);
	$c_stmt		= $db->pdo->prepare('SELECT * FROM kelas WHERE kode_kelas = "'.$_GET['kode_kelas'].'"');
	$c_stmt->execute();
	$c_data 	= $c_stmt->fetch(PDO::FETCH_OBJ);
	$g_stmt		= $db->pdo->prepare('SELECT * FROM guru WHERE nip = "'.$_SESSION['nip'].'"');
	$g_stmt->execute();
	$g_data 	= $g_stmt->fetch(PDO::FETCH_OBJ);
	$m_stmt		= $db->pdo->prepare('SELECT standar_kompetensi.nama_sk, mata_diklat.nama_mata_diklat FROM standar_kompetensi INNER JOIN mata_diklat ON mata_diklat.kode_mata_diklat = standar_kompetensi.kode_mata_diklat WHERE kode_sk = "'.$_GET['kode_sk'].'"');
	$m_stmt->execute();
	$m_data 	= $m_stmt->fetch(PDO::FETCH_OBJ);
?>
<h2 class="page-title">Input Nilai</h2>
<div class="half" style="margin-left: -20px; margin-right: 20px">
	<label>Kompetensi Keahlian</label>
	<input type="text" value="<?= $k_data->nama_kk; ?>" disabled />
	<label>Kelas</label>
	<input type="text" value="<?= $c_data->nama_kelas; ?>" disabled />
</div>
<div class="half">
	<label>Mata Diklat</label>
	<input type="text" value="<?= $m_data->nama_mata_diklat; ?>" disabled />
	<label>Standar Kompetensi</label>
	<input type="text" value="<?= $m_data->nama_sk; ?>" disabled />
</div>
<div class="clear"></div>
<?php
	$sql 	=	'SELECT
					*
				FROM
					nilai
				INNER JOIN siswa ON
					siswa.nisn = nilai.nisn
				INNER JOIN kelas ON
					siswa.kode_kelas = kelas.kode_kelas
				WHERE
					kelas.kode_kelas = "'.$_GET['kode_kelas'].'" AND nilai.kode_sk = "'.$_GET['kode_sk'].'"
				';
	$stmt	= $db->pdo->prepare($sql);
	$stmt->execute();
	if ($stmt->rowCount() == 0):
?>
<table class="data-table">
	<thead>
		<tr>
			<th>NISN</th>
			<th>Nama Siswa</th>
			<th>Nilai</th>
		</tr>
	</thead>
	<tbody>
		<form id="inputform" action="modules/insertData.php" method="post">
		<input type="hidden" name="data_type" value="nilai" />
		<input type="hidden" name="kode_sk" value="<?= $_GET['kode_sk']; ?>" />
		<input type="hidden" name="nip" value="<?= $_SESSION['nip']; ?>" />
		<?php
			$stmt	= $db->pdo->prepare('SELECT * FROM siswa WHERE kode_kelas = "'.$_GET['kode_kelas'].'"');
			$stmt->execute();
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<td width="150px"><?= $row['nisn']; ?><input type="hidden" name="nisn[]" value="<?= $row['nisn']; ?>" /></td>
				<td width="700px"><?= $row['nama_siswa']; ?></td>
				<td><input type="number" name="nilai_angka[]" /></td>
			</tr>
		<?php endwhile; ?>
		</form>
	</tbody>
</table>
<button type="button" class="btn" onclick="document.getElementById('inputform').submit()" style="float: right;margin-top: 20px">Simpan</button>
<?php else: ?>
<table class="data-table">
	<thead>
		<tr>
			<th>NISN</th>
			<th>Nama Siswa</th>
			<th>Nilai</th>
		</tr>
	</thead>
	<tbody>
		<form id="inputform" action="modules/insertData.php" method="post">
		<input type="hidden" name="data_type" value="nilai" />
		<input type="hidden" name="kode_sk" value="<?= $_GET['kode_sk']; ?>" />
		<input type="hidden" name="nip" value="<?= $_SESSION['nip']; ?>" />
		<?php
			$sql 	=	'SELECT
							nilai.*,
							siswa.nama_siswa
						FROM
							nilai
						INNER JOIN siswa ON
							siswa.nisn = nilai.nisn
						INNER JOIN kelas ON
							siswa.kode_kelas = kelas.kode_kelas
						WHERE
							kelas.kode_kelas = "'.$_GET['kode_kelas'].'" AND nilai.kode_sk = "'.$_GET['kode_sk'].'"
						';
			$stmt->execute();
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<td width="150px"><?= $row['nisn']; ?><input type="hidden" name="nisn[]" value="<?= $row['nisn']; ?>" /></td>
				<td width="700px"><?= $row['nama_siswa']; ?></td>
				<td><input type="number" name="nilai_angka[]" value="<?= $row['nilai_angka']; ?>" /></td>
			</tr>
		<?php endwhile; ?>
		</form>
	</tbody>
</table>
<button type="button" class="btn" onclick="document.getElementById('inputform').submit()" style="float: right;margin-top: 20px">Simpan</button>
<?php endif; ?>