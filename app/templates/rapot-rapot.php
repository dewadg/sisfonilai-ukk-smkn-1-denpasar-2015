<div class="print">
<h2 class="page-title">Rapot Siswa</h2>
<button style="margin-bottom: 20px" type="button" class="btn" onclick="window.location.href='?page=rapot-print-out&kode_kk=<?= $_GET['kode_kk']; ?>&kode_kelas=<?= $_GET['kode_kelas']; ?>&nisn=<?= $_GET['nisn']; ?>'">Cetak</button>
<table class="rapot-head">
	<tbody>
		<tr>
			<td colspan="2">
				<img src="http://localhost/22791/app/uploads/logo-rapot.png" />
				<header>
					<h1>SMK PGRI 3 Badung</h1>
					<p align="center">Alamat: Jalan Wayan Gebyag Nomor 5 Tegaljaya, Dalung - Telepon: 0361 8535885</p>
					<p align="center">Website: www.smkpgri3badung.sch.id - Email: info@smkpgri3badung.sch.id</p>
				</header>
			</td>
		</tr>
	</tbody>
</table>
<table class="rapot-head" style="border-top: none;">	
	<?php
		$sql 	=	'SELECT
						siswa.*,
						wali_murid.nama_ayah,
						kompetensi_keahlian.nama_kk,
						kelas.nama_kelas,
						guru.nama_guru
					FROM
						siswa
					INNER JOIN wali_murid ON
						wali_murid.kode_wali = siswa.kode_wali
					INNER JOIN kompetensi_keahlian ON
						siswa.kode_kk = kompetensi_keahlian.kode_kk
					INNER JOIN kelas ON
						siswa.kode_kelas = kelas.kode_kelas
					INNER JOIN guru ON
						kelas.kode_wali_kelas = guru.nip
					WHERE
						siswa.nisn = "'.$_GET['nisn'].'"
					';
		$rh_stmt = $db->pdo->prepare($sql);
		$rh_stmt->execute();
		$rh_data = $rh_stmt->fetch(PDO::FETCH_ASSOC);
	?>
	<tbody>
		<tr>
			<th>NISN</th>
			<td><?= $_GET['nisn']; ?></td>
			<th>Kompetensi Keahlian</th>
			<td><?= $rh_data['nama_kk']; ?></td>
		</tr>
		<tr>
			<th>Nama Siswa</th>
			<td><?= $rh_data['nama_siswa']; ?></td>
			<th>Kelas</th>
			<td><?= $rh_data['nama_kelas']; ?></td>
		</tr>
		<tr>
			<th>Wali Siswa</th>
			<td><?= $rh_data['nama_ayah']; ?></td>
			<th>Wali Kelas</th>
			<td><?= $rh_data['nama_guru']; ?></td>
		</tr>
	</tbody>
</table>
<table class="rapot-body" style="border-top: none;">
	<tbody>
		<tr>
			<th rowspan="2" style="border-right: 1px solid #000"><center>Mata Diklat</center></th>
			<th rowspan="2" style="border-right: 1px solid #000"><center>KKM</center></th>
			<th colspan="2" style="border-right: 1px solid #000"><center>Nilai</center></th>
		</tr>
		<tr>
			<th style="border-right: 1px solid #000"><center>Angka</center></th>
			<th style="border-right: 1px solid #000"><center>Huruf</center></th>
		</tr>
		<?php
			$kmd_stmt	= $db->pdo->prepare('SELECT * FROM kategori_mata_diklat');
			$kmd_stmt->execute();
			$kkm_total = 0;
			$nilai_total = 0;
			while ($kmd_row = $kmd_stmt->fetch(PDO::FETCH_ASSOC)):
		?>
			<tr>
				<th colspan="5"><?= $kmd_row['nama_kategori']; ?></th>
			</tr>
			<?php
				$md_stmt	= $db->pdo->prepare('SELECT * FROM mata_diklat WHERE kode_kategori = "'.$kmd_row['kode_kategori'].'" AND kode_kk IN("2", "'.$_GET['kode_kk'].'")');
				$md_stmt->execute();
				while ($md_row = $md_stmt->fetch(PDO::FETCH_ASSOC)):
					$n_stmt	= $db->pdo->prepare('CALL countScore("'.$md_row['kode_mata_diklat'].'", "'.$_GET['nisn'].'")');
					$n_stmt->execute();
					$n_data = $n_stmt->fetch(PDO::FETCH_ASSOC);
					$kkm_total = $kkm_total + $md_row['kkm'];
					$nilai_total = $nilai_total + $n_data['nilai'];
			?>
				<tr>
					<td><?= $md_row['nama_mata_diklat']; ?></td>
					<td><center><?= $md_row['kkm']; ?></center></td>
					<td><center><?= $n_data['nilai']; ?></center></td>
					<td style="border-right: 1px solid #000;"><center><?= convertScore($n_data['nilai']); ?></center></td>
				</tr>
			<?php endwhile; ?>
		<?php endwhile;?>
		<tr>
			<th style="text-align: right; border-right: 1px solid #000;">Jumlah Nilai:</th>
			<td><center><?= $kkm_total; ?></center></td>
			<td colspan="2" style="border-right: 1px solid #000;"><center><?= $nilai_total; ?></center></td>
		</tr>
		<tr>
			<td colspan="5" style="font-size: 13px;"><center>Copyright &copy; 2015 SMK PGRI 3 Badung</center></td>
		</tr>
	</tbody>
</table>
</div>