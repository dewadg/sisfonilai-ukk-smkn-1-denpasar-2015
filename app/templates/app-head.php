<?php startSession(); ?>
<html>
<head>
<title><?= $page; ?></title>
<link rel="stylesheet" href="<?= baseUrl('templates/style.css'); ?>" type="text/css" />
<link rel="stylesheet" href="<?= baseUrl('templates/font-awesome/css/font-awesome.min.css'); ?>" type="text/css" />
</head>

<body>
<section class="sidebar">
	<h1 class="site-logo"><a href="<?= baseUrl(); ?>" title="SMK PGRI 3 Badung">SMK PGRI 3 Badung</a></h1>
	<nav class="site-menu">
		<ul>
			<?php if (isUser('admin')): ?>
				<li>
					<a href="#"><i class="fa fa-users"></i>Siswa</a>
					<ul class="sub-menu">
						<li><a href="?page=data-siswa">Data Siswa</a></li>
						<li><a href="?page=data-wali">Data Wali</a></li>
						<li><a href="?page=data-kelas">Data Kelas</a></li>
					</ul>
				</li>
				<li>
					<a href="?page=data-guru"><i class="fa fa-users"></i>Guru</a>
				</li>
				<li>
					<a href="#"><i class="fa fa-university"></i>Sistem Pendidikan</a>
					<ul class="sub-menu">
						<li><a href="?page=data-kk">Kompetensi Keahlian</a></li>
						<li><a href="?page=data-md">Mata Diklat</a></li>
						<li><a href="?page=data-sk">Standar Kompetensi</a></li>
					</ul>
				</li>
				<li>
					<a href="modules/backupDatabase.php"><i class="fa fa-database"></i>Backup Database</a>
				</li>
			<?php endif; ?>
			<li>
				<a href="#"><i class="fa fa-book"></i>Laporan Hasil Belajar</a>
				<ul class="sub-menu">
					<?php if (isUser('guru')): ?>
						<li><a href="?page=nilai">Input Nilai</a></li>
					<?php endif; ?>
					<?php if (!isUser('siswa')): ?>
						<li><a href="?page=rapot">Cetak Rapot</a></li>
					<?php else: ?>
						<li><a href="?page=rapot&kode_kk=<?= $_SESSION['kode_kk']; ?>&kode_kelas=<?= $_SESSION['kode_kelas']; ?>&nisn=<?= $_SESSION['nisn']; ?>">Cetak Rapot</a></li>
					<?php endif; ?>
				</ul>
			</li>
			<li>
				<a href="#"><i class="fa fa-user"></i><?= $_SESSION['user']; ?> (<?= $_SESSION['user_type']; ?>)</a>
				<ul class="sub-menu">	
					<li><a href="modules/doLogout.php">Log out</a></li>
				</ul>
			</li>
		</ul>
	</nav>
</section>
<section class="content">