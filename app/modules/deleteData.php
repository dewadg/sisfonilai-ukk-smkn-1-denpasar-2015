<?php

include_once('../functions.php');
if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
$db	= new Database();

switch ($_GET['data_type']):
	case 'kk':
		$stmt	= $db->pdo->prepare('DELETE FROM kompetensi_keahlian WHERE kode_kk = "'.$_GET['kode_kk'].'"');
		if ($stmt->execute()):
			switchPage('data-kk&message=6');
		else:
			switchPage('data-kk&message=7');
		endif;
	break;
	case 'md':
		$stmt	= $db->pdo->prepare('DELETE FROM mata_diklat WHERE kode_mata_diklat = "'.$_GET['kode_mata_diklat'].'"');
		if ($stmt->execute()):
			switchPage('data-md&message=6');
		else:
			switchPage('data-md&message=7');
		endif;
	break;
	case 'sk':
		$stmt 	= $db->pdo->prepare('DELETE FROM standar_kompetensi WHERE kode_sk = "'.$_GET['kode_sk'].'"');
		if ($stmt->execute()):
			switchPage('data-sk&message=6');
		else:
			switchPage('data-sk&message=7');
		endif;
	break;
	case 'guru':	
		$stmt 	= $db->pdo->prepare('DELETE FROM guru WHERE nip = "'.$_GET['nip'].'"');
		if ($stmt->execute()):
			switchPage('data-guru&message=6');
		else:
			switchPage('data-guru&message=7');
		endif;
	break;
	case 'kelas':
		$stmt 	= $db->pdo->prepare('DELETE FROM kelas WHERE kode_kelas = "'.$_GET['kode_kelas'].'"');
		if ($stmt->execute()):
			switchPage('data-kelas&message=6');
		else:
			switchPage('data-kelas&message=7');
		endif;
	break;
	case 'wali':
		$stmt 	= $db->pdo->prepare('DELETE FROM wali_murid WHERE kode_wali = "'.$_GET['kode_wali'].'"');
		if ($stmt->execute()):
			switchPage('data-wali&message=6');
		else:
			switchPage('data-wali&message=7');
		endif;
	break;
	case 'siswa':
		$stmt	= $db->pdo->prepare('SELECT nama_foto FROM siswa WHERE nisn = "'.$_GET['nisn'].'"');
		$stmt->execute();
		$data 	= $stmt->fetch(PDO::FETCH_ASSOC);
		if (file_exists('../uploads/'.$data['nama_foto'])):
			unlink('../uploads/'.$data['nama_foto']);
		endif;
		$stmt	= $db->pdo->prepare('DELETE FROM siswa WHERE nisn = "'.$_GET['nisn'].'"');
		if ($stmt->execute()):
			switchPage('data-siswa&message=6');
		else:
			switchPage('data-siswa&message=7');
		endif;
	break;
endswitch;

?>