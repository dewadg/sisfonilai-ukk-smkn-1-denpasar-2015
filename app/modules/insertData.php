<?php

include_once('../functions.php');
if (!isLogged()): switchPage('index&message=1'); endif;
$db	= new Database();

switch ($_POST['data_type']):
	case 'kk':
		$stmt	= $db->pdo->prepare('INSERT INTO kompetensi_keahlian SET nama_kk = :nama_kk');
		$vals	= array(':nama_kk' => $_POST['nama_kk']);
		if ($stmt->execute($vals)):
			switchPage('tambah-kk&message=4');
		else:
			switchPage('tambah-kk&message=5');
		endif;
	break;
	case 'md':
		$sql 	=	'INSERT INTO
						mata_diklat
					SET
						nama_mata_diklat	= :nama_mata_diklat,
						kode_kk				= :kode_kk,
						kkm					= :kkm,
						kode_kategori		= :kode_kategori
					';
		$stmt	= $db->pdo->prepare($sql);
		$vals	=	array(
						':nama_mata_diklat'	=> $_POST['nama_mata_diklat'],
						':kode_kk'			=> $_POST['kode_kk'],
						':kkm'				=> $_POST['kkm'],
						':kode_kategori'	=> $_POST['kode_kategori']
					);
		if ($stmt->execute($vals)):
			switchPage('tambah-md&message=4');
		else:
			switchPage('tambah-md&message=5');
		endif;
	break;
	case 'sk':
		$sql 	=	'INSERT INTO
						standar_kompetensi
					SET
						nama_sk				= :nama_sk,
						kode_mata_diklat	= :kode_mata_diklat
					';
		$stmt	= $db->pdo->prepare($sql);
		$vals	=	array(
						':nama_sk'			=> $_POST['nama_sk'],
						':kode_mata_diklat'	=> $_POST['kode_mata_diklat']
					);
		if ($stmt->execute($vals)):
			switchPage('tambah-sk&message=4');
		else:
			switchPage('tambah-sk&message=5');
		endif;
	break;
	case 'guru':
		$stmt	= $db->pdo->prepare('SELECT * FROM guru WHERE nip = "'.$_POST['nip'].'"');
		$stmt->execute();
		if ($stmt->rowCount() > 0):
			switchPage('tambah-guru&message=10');
		else:
			$sql 	=	'INSERT INTO
							guru
						SET
							nip					= :nip,
							nama_guru			= :nama_guru,
							alamat_guru			= :alamat_guru,
							telp_guru			= :telp_guru,
							kode_mata_diklat	= :kode_mata_diklat,
							user 				= :user,
							password			= :password,
							kode_kk				= :kode_kk
						';
			$stmt 	= $db->pdo->prepare($sql);
			$vals 	=	array(	
							':nip'				=> $_POST['nip'],
							':nama_guru'		=> $_POST['nama_guru'],
							':alamat_guru'		=> $_POST['alamat_guru'],
							':telp_guru'		=> $_POST['telp_guru'],
							':kode_mata_diklat'	=> $_POST['kode_mata_diklat'],
							':user'				=> $_POST['user'],
							':password' 		=> $_POST['password'],
							':kode_kk'			=> $_POST['kode_kk']
						);
			if ($stmt->execute($vals)):
				switchPage('tambah-guru&message=4');
			else:
				switchPage('tambah-guru&message=5');
			endif;
		endif;
	break;
	case 'kelas':
		$sql 	=	'INSERT INTO
						kelas
					SET
						nama_kelas 		= :nama_kelas,
						kode_kk			= :kode_kk,
						kode_wali_kelas	= :kode_wali_kelas
					';
		$stmt 	= $db->pdo->prepare($sql);
		$vals 	=	array(
						':nama_kelas'		=> $_POST['nama_kelas'],
						':kode_kk'			=> $_POST['kode_kk'],
						':kode_wali_kelas'	=> $_POST['kode_wali_kelas']
					);
		if ($stmt->execute($vals)):
			switchPage('tambah-kelas&message=4');
		else:
			switchPage('tambah-kelas&message=5');
		endif;
	break;
	case 'wali':
		$sql 	=	'INSERT INTO
						wali_murid
					SET
						nama_ayah		= :nama_ayah,
						pekerjaan_ayah	= :pekerjaan_ayah,
						nama_ibu		= :nama_ibu,
						pekerjaan_ibu	= :pekerjaan_ibu,
						alamat_wali		= :alamat_wali,
						telp_wali		= :telp_wali
					';
		$stmt 	= $db->pdo->prepare($sql);
		$vals	=	array(
						':nama_ayah'		=> $_POST['nama_ayah'],
						':pekerjaan_ayah'	=> $_POST['pekerjaan_ayah'],
						':nama_ibu'			=> $_POST['nama_ibu'],
						':pekerjaan_ibu'	=> $_POST['pekerjaan_ibu'],
						':alamat_wali'		=> $_POST['alamat_wali'],
						':telp_wali'		=> $_POST['telp_wali']
					);
		if ($stmt->execute($vals)):
			switchPage('tambah-wali&message=4');
		else:
			switchPage('tambah-wali&message=5');
		endif;
	break;
	case 'siswa':
		$stmt 	= $db->pdo->prepare('SELECT * FROM siswa WHERE nisn = "'.$_POST['nisn'].'"');
		$stmt->execute();
		if ($stmt->rowCount() > 0):
			switchPage('tambah-siswa&message=11');
		else:
			$photo		= $_FILES['foto']['name'];
			$photo_set	= explode('.', $photo);
			$photo_ext	= end($photo_set);
			$photo_dir	= '../uploads/foto_'.$_POST['nisn'].'.'.$photo_ext;
			$photo_name	= 'foto_'.$_POST['nisn'].'.'.$photo_ext;
			$photo_url	= 'http://localhost/22791/app/uploads/'.$photo_name;
			move_uploaded_file($_FILES['foto']['tmp_name'], $photo_dir);
			
			$sql		=	'INSERT INTO
								siswa
							SET
								nisn			= :nisn,
								nama_siswa		= :nama_siswa,
								alamat_siswa	= :alamat_siswa,
								tgl_lahir		= :tgl_lahir,
								url_foto		= :url_foto,
								nama_foto		= :nama_foto,
								kode_kk			= :kode_kk,
								kode_kelas		= :kode_kelas,
								kode_wali		= :kode_wali,
								user			= :user,
								password		= :password
							';
			$stmt		= $db->pdo->prepare($sql);
			$vals		=	array(
								':nisn'			=> $_POST['nisn'],
								':nama_siswa'	=> $_POST['nama_siswa'],
								':alamat_siswa'	=> $_POST['alamat_siswa'],
								':tgl_lahir'	=> $_POST['tgl_lahir'],
								':url_foto'		=> $photo_url,
								':nama_foto'	=> $photo_name,
								':kode_kk'		=> $_POST['kode_kk'],
								':kode_kelas'	=> $_POST['kode_kelas'],
								':kode_wali'	=> $_POST['kode_wali'],
								':user'			=> $_POST['user'],
								':password'		=> $_POST['password']
							);
			if ($stmt->execute($vals)):
				switchPage('tambah-siswa&message=4');
			else:
				switchPage('tambah-siswa&message=5');
			endif;
		endif;
	break;
	case 'nilai':
		$num 	= 0;
		$sql 	=	'INSERT INTO
						nilai
					SET
						nisn 		= :nisn,
						nip			= :nip,
						kode_sk		= :kode_sk,
						nilai_angka	= :nilai_angka,
						nilai_huruf	= :nilai_huruf
					';
		foreach ($_POST['nisn'] as $nisn):
			$status = array();
			$vals 	=	array(
							':nisn'			=> $nisn,
							':nip'			=> $_POST['nip'],
							':kode_sk'		=> $_POST['kode_sk'],
							':nilai_angka'	=> $_POST['nilai_angka'][$num],
							':nilai_huruf'	=> convertScore($_POST['nilai_angka'][$num])
						);
			$stmt 	= $db->pdo->prepare($sql);
			if ($stmt->execute($vals)):
				$status[] = 1;
			else:
				$status[] = 0;
			endif;
			$num++;
		endforeach;
		if (in_array(0, $status)):
			switchPage('nilai&message=13');
		else:
			switchPage('nilai&message=12');
		endif;
	break;
endswitch;

?>