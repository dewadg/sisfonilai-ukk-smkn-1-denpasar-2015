<?php

include_once('../functions.php');

if (!isUser('admin')):
	switchPage('main');
else:
	if (file_exists('../uploads/backup_db.sql')):
		unlink('../uploads/backup_db.sql');
	endif;
	$cmd	= 'C:\xampp\mysql\bin\mysqldump -u root 22791_ukk > ../downloads/backup_db.sql';
	exec($cmd);
	header('location:../downloads/backup_db.sql');
endif;

?>