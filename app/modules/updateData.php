<?php

include_once('../functions.php');
if (!isLogged()): switchPage('index&message=1'); else: if (!isUser('admin')): switchPage('main&message=3'); endif; endif;
$db	= new Database();

switch ($_POST['data_type']):
	case 'kk':
		$stmt	= $db->pdo->prepare('UPDATE kompetensi_keahlian SET nama_kk = :nama_kk WHERE kode_kk = "'.$_POST['kode_kk'].'"');
		$vals	= array(':nama_kk' => $_POST['nama_kk']);
		if ($stmt->execute($vals)):
			switchPage('data-kk&message=8');
		else:
			switchPage('data-kk&message=9');
		endif;
	break;
	case 'md':
		$sql	=	'UPDATE
						mata_diklat
					SET
						nama_mata_diklat	= :nama_mata_diklat,
						kode_kk 			= :kode_kk,
						kkm					= :kkm,
						kode_kategori		= :kode_kategori
					WHERE
						kode_mata_diklat	= "'.$_POST['kode_mata_diklat'].'"
					';
		$stmt	= $db->pdo->prepare($sql);
		$vals 	=	array(
						':nama_mata_diklat'	=> $_POST['nama_mata_diklat'],
						':kode_kk'			=> $_POST['kode_kk'],
						':kkm'				=> $_POST['kkm'],
						':kode_kategori'	=> $_POST['kode_kategori']
					);	
		if ($stmt->execute($vals)):
			switchPage('data-md&message=8');
		else:
			switchPage('data-md&message=9');
		endif;
	break;
	case 'sk':
		$sql 	=	'UPDATE
						standar_kompetensi
					SET
						nama_sk				= :nama_sk,
						kode_mata_diklat	= :kode_mata_diklat
					WHERE
						kode_sk				= "'.$_POST['kode_sk'].'"
					';
		$stmt	= $db->pdo->prepare($sql);
		$vals 	=	array(
						':nama_sk'			=> $_POST['nama_sk'],
						':kode_mata_diklat'	=> $_POST['kode_mata_diklat']
					);
		if ($stmt->execute($vals)):
			switchPage('data-sk&message=8');
		else:
			switchPage('data-sk&message=9');
		endif;
	break;
	case 'guru':
		$sql 	=	'UPDATE
						guru
					SET
						nama_guru			= :nama_guru,
						alamat_guru			= :alamat_guru,
						telp_guru			= :telp_guru,
						kode_mata_diklat	= :kode_mata_diklat,
						user 				= :user,
						password			= :password,
						kode_kk				= :kode_kk
					WHERE
						nip					= "'.$_POST['nip'].'"
					';
		$stmt 	= $db->pdo->prepare($sql);
		$vals 	=	array(	
						':nama_guru'		=> $_POST['nama_guru'],
						':alamat_guru'		=> $_POST['alamat_guru'],
						':telp_guru'		=> $_POST['telp_guru'],
						':kode_mata_diklat'	=> $_POST['kode_mata_diklat'],
						':user'				=> $_POST['user'],
						':password' 		=> $_POST['password'],
						':kode_kk'			=> $_POST['kode_kk']
					);
		if ($stmt->execute($vals)):
			switchPage('data-guru&message=8');
		else:
			switchPage('data-guru&message=9');
		endif;
	break;
	case 'kelas':
		$sql 	=	'UPDATE
						kelas
					SET
						nama_kelas		= :nama_kelas,
						kode_kk			= :kode_kk,
						kode_wali_kelas	= :kode_wali_kelas
					WHERE
						kode_kelas 		= "'.$_POST['kode_kelas'].'"
					';
		$stmt 	= $db->pdo->prepare($sql);
		$vals	= 	array(
						':nama_kelas'		=> $_POST['nama_kelas'],
						':kode_kk'			=> $_POST['kode_kk'],
						':kode_wali_kelas'	=> $_POST['kode_wali_kelas']
					);
		if ($stmt->execute($vals)):
			switchPage('data-kelas&message=8');
		else:
			switchPage('data-kelas&message=9');
		endif;
	break;
	case 'wali':
		$sql 	=	'UPDATE	
						wali_murid
					SET
						nama_ayah		= :nama_ayah,
						pekerjaan_ayah	= :pekerjaan_ayah,
						nama_ibu		= :nama_ibu,
						pekerjaan_ibu	= :pekerjaan_ibu,
						alamat_wali		= :alamat_wali,
						telp_wali		= :telp_wali
					WHERE
						kode_wali 		= "'.$_POST['kode_wali'].'"
					';
		$stmt 	= $db->pdo->prepare($sql);
		$vals	= 	array(
						':nama_ayah'		=> $_POST['nama_ayah'],
						':pekerjaan_ayah'	=> $_POST['pekerjaan_ayah'],
						':nama_ibu'			=> $_POST['nama_ibu'],
						':pekerjaan_ibu'	=> $_POST['pekerjaan_ibu'],
						':alamat_wali'		=> $_POST['alamat_wali'],
						':telp_wali'		=> $_POST['telp_wali']
					);
		if ($stmt->execute($vals)):
			switchPage('data-wali&message=8');
		else:
			switchPage('data-wali&message=9');
		endif;
	break;
	case 'siswa':
		if (isset($_FILES['foto'])):
			$photo		= $_FILES['foto']['name'];
			$photo_set	= explode('.', $photo);
			$photo_ext	= end($photo_set);
			$photo_dir	= '../uploads/foto_'.$_POST['nisn'].'.'.$photo_ext;
			$photo_name	= 'foto_'.$_POST['nisn'].'.'.$photo_ext;
			$photo_url	= 'http://localhost/22791/app/uploads/'.$photo_name;
			if (file_exists($photo_dir)):
				unlink($photo_dir);
			endif;
			move_uploaded_file($_FILES['foto']['tmp_name'], $photo_dir);
		else:
			$photo_name = null;
			$photo_url	= null;
		endif;
		
		if (!empty($photo_name) && !empty($photo_url)):
			$sql	=	'UPDATE
							siswa
						SET
							nama_siswa		= :nama_siswa,
							alamat_siswa	= :alamat_siswa,
							tgl_lahir		= :tgl_lahir,
							url_foto		= :url_foto,
							nama_foto		= :nama_foto,
							kode_kk			= :kode_kk,
							kode_kelas		= :kode_kelas,
							kode_wali		= :kode_wali,
							user			= :user,
							password		= :password
						WHERE
							nisn 			= "'.$_POST['nisn'].'"
						';
			$vals	=	array(
							':nama_siswa'	=> $_POST['nama_siswa'],
							':alamat_siswa'	=> $_POST['alamat_siswa'],
							':tgl_lahir'	=> $_POST['tgl_lahir'],
							':url_foto'		=> $photo_url,
							':nama_foto'	=> $photo_name,
							':kode_kk'		=> $_POST['kode_kk'],
							':kode_kelas'	=> $_POST['kode_kelas'],
							':kode_wali'	=> $_POST['kode_wali'],
							':user'			=> $_POST['user'],
							':password'		=> $_POST['password']
						);
		else:
			$sql	=	'UPDATE
							siswa
						SET
							nama_siswa		= :nama_siswa,
							alamat_siswa	= :alamat_siswa,
							tgl_lahir		= :tgl_lahir,
							kode_kk			= :kode_kk,
							kode_kelas		= :kode_kelas,
							kode_wali		= :kode_wali,
							user			= :user,
							password		= :password
						WHERE
							nisn 			= "'.$_POST['nisn'].'"
						';
			$vals	=	array(
							':nama_siswa'	=> $_POST['nama_siswa'],
							':alamat_siswa'	=> $_POST['alamat_siswa'],
							':tgl_lahir'	=> $_POST['tgl_lahir'],
							':kode_kk'		=> $_POST['kode_kk'],
							':kode_kelas'	=> $_POST['kode_kelas'],
							':kode_wali'	=> $_POST['kode_wali'],
							':user'			=> $_POST['user'],
							':password'		=> $_POST['password']
						);
		endif;
		
		$stmt			= $db->pdo->prepare($sql);		
		if ($stmt->execute($vals)):
			switchPage('data-siswa&message=8');
		else:
			switchPage('data-siswa&message=8');
		endif;
	break;
endswitch;

?>