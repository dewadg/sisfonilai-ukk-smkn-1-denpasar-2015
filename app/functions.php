<?php

function __autoload($class) {
	include_once('classes/'.$class.'.php');
}

function baseUrl($dir = null) {
	if (empty($dir)):
		return 'http://localhost/22791/app';
	else:
		return 'http://localhost/22791/app/'.$dir;
	endif;
}

function appIndex() {
	if (!isset($_GET['page']) || empty($_GET['page']) || $_GET['page'] == 'index'):
		include_once('templates/app-index.php');
	else:
		if (!file_exists('templates/'.$_GET['page'].'.php')):
			include_once('templates/404.php');
		else:
			include_once('templates/'.$_GET['page'].'.php');
		endif;
	endif;
}

function messageAlert() {
	$message =	array(
					1	=> 'Mohon login terlebih dahulu',
					2 	=> 'Login gagal. Cek user/password kembali',
					3	=> 'Anda tidak memiliki hak akses ke halaman ini',
					4 	=> 'Data berhasil ditambahkan',
					5	=> 'Data gagal ditambahkan',
					6	=> 'Data berhasil dihapus',
					7	=> 'Data gagal dihapus',
					8	=> 'Data berhasil diperbarui',
					9	=> 'Data gagal diperbarui',
					10	=> 'NIP sudah terdaftar',
					11	=> 'NISN sudah terdaftar',
					12 	=> 'Nilai berhasil diinputkan',
					13 	=> 'Nilai gagal diinputkan'
				);
	if (isset($_GET['message']) && !empty($_GET['message'])):
		echo '<script type="text/javascript">';
		echo 'alert("'.$message[$_GET['message']].'");';
		echo '</script>';
	endif;
}

function startSession() {
	if (session_status() == PHP_SESSION_NONE):
		session_start();
	endif;
}

function switchPage($page) {
	header('location:http://localhost/22791/app/?page='.$page);
}

function isLogged() {
	startSession();
	if ($_SESSION['auth'] != 1):
		return false;
	else:
		return true;
	endif;
}

function isUser($user) {
	startSession();
	switch ($user):
		case 'admin':
			if ($_SESSION['user_type'] == 'admin'):
				return true;
			else:
				return false;
			endif;
		break;
		case 'guru':
			if ($_SESSION['user_type'] == 'guru'):
				return true;
			else:
				return false;
			endif;
		break;
		case 'siswa':
			if ($_SESSION['user_type'] == 'siswa'):
				return true;
			else:
				return false;
			endif;
		break;
	endswitch;
}

function checkPar($par) {
	if (isset($par) && !empty($par)):
		return true;
	else:
		return false;
	endif;
}

function matchVals($first, $second, $output) {
	if ($first == $second):	return $output; endif;
}

function convertScore($val) {
	if ($val >= 90 && $val <= 100):
		return 'A';
	elseif ($val >= 80):
		return 'B';
	elseif ($val >= 70):
		return 'C';
	elseif ($val >= 60):
		return 'D';
	elseif ($val >= 50):
		return 'E';
	elseif ($val <= 49):
		return 'F';
	endif;
}

?>