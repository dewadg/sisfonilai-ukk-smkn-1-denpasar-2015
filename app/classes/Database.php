<?php

class Database {
	public $pdo;
	
	public function __construct() {
		$this->pdo	= new PDO('mysql:host=localhost;port=3306;dbname=22791_ukk', 'root', null);
	}
	
	public function pdo() {
		return $this->pdo;
	}
	
	public function fetch($table, $order_col, $order) {
		if (empty($where)):
			$stmt	= $this->pdo->prepare('SELECT * FROM '.$table.' ORDER BY '.$order_col.' '.$order);	
		else:
			$stmt	= $this->pdo->prepare('SELECT * FROM '.$table.' WHERE '.$where.' ORDER BY '.$order_col.' '.$order);
		endif;
		$stmt->execute();
		$output	= array();
		while ($data = $stmt->fetch(PDO::FETCH_ASSOC)):
			$output[] = $data;
		endwhile;
		return $output;		
	}
}

?>