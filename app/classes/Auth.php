<?php

class Auth {
	private $db;
	
	public function __construct() {
		$this->db	= new Database();
	}
	
	public function login($user, $password, $target) {
		$sql	= 'SELECT * FROM '.$target.' WHERE user = :user AND password = :password';
		$vals	= array(':user' => $user, ':password' => $password);
		$stmt	= $this->db->pdo->prepare($sql);
		$stmt->execute($vals);
		
		startSession();
		if ($stmt->rowCount() == 0):
			switchPage('index&message=2');
		else:
			$data				= $stmt->fetch(PDO::FETCH_ASSOC);
			$_SESSION['auth']	= 1;
			foreach ($data as $key => $val):
				$_SESSION[$key] = $val;
			endforeach;
			switchPage('main');
		endif;
	}
	
	public function logout() {
		startSession();
		session_unset();
		session_destroy();
		switchPage('index');
	}
}

?>