-- MySQL dump 10.13  Distrib 5.6.14, for Win32 (x86)
--
-- Host: localhost    Database: 22791_ukk
-- ------------------------------------------------------
-- Server version	5.6.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `guru`
--

DROP TABLE IF EXISTS `guru`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guru` (
  `nip` varchar(5) NOT NULL,
  `kode_kk` int(11) NOT NULL,
  `nama_guru` varchar(50) NOT NULL,
  `alamat_guru` text NOT NULL,
  `telp_guru` varchar(20) NOT NULL,
  `user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `user_type` enum('guru','','','') NOT NULL DEFAULT 'guru',
  `kode_mata_diklat` int(11) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guru`
--

LOCK TABLES `guru` WRITE;
/*!40000 ALTER TABLE `guru` DISABLE KEYS */;
INSERT INTO `guru` VALUES ('66001',5,'Sadia Permana','Denpasar','087860999888','sadia','sadia','guru',5),('66002',2,'Ida Bagus Oka','Mengwi','081765787678','gusoka','gusoka','guru',1),('66003',2,'Dwi Jayanti','Sempidi','0897890986543','dwijayanti','dwijayanti','guru',2),('66004',2,'Dodik Kustanto','Banyuwangi','089787678789','dodik','dodik','guru',3),('66005',6,'Karmayasa','Denpasar','087878767667','karma','karma','guru',6),('66006',5,'Putri Suharsiki','Gianyar','087676768788','putri','putri','guru',6),('66007',2,'Ngurah Bagus','Tabanan','085678987687','ngurah','ngurah','guru',7),('66008',7,'Yoga Nugraha','Jl. I Gede Desa 1','089787678789','yoganug','yoganug','guru',10);
/*!40000 ALTER TABLE `guru` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori_mata_diklat`
--

DROP TABLE IF EXISTS `kategori_mata_diklat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori_mata_diklat` (
  `kode_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(20) NOT NULL,
  PRIMARY KEY (`kode_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori_mata_diklat`
--

LOCK TABLES `kategori_mata_diklat` WRITE;
/*!40000 ALTER TABLE `kategori_mata_diklat` DISABLE KEYS */;
INSERT INTO `kategori_mata_diklat` VALUES (1,'Normatif/Adaptif'),(2,'Produktif'),(3,'Muatan Lokal');
/*!40000 ALTER TABLE `kategori_mata_diklat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kelas`
--

DROP TABLE IF EXISTS `kelas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kelas` (
  `kode_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kk` int(11) NOT NULL,
  `nama_kelas` varchar(20) NOT NULL,
  `kode_wali_kelas` varchar(5) NOT NULL,
  PRIMARY KEY (`kode_kelas`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kelas`
--

LOCK TABLES `kelas` WRITE;
/*!40000 ALTER TABLE `kelas` DISABLE KEYS */;
INSERT INTO `kelas` VALUES (1,5,'X RPL I','66001'),(3,6,'X MM I','66004'),(4,7,'X TKJ I','66002');
/*!40000 ALTER TABLE `kelas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kompetensi_keahlian`
--

DROP TABLE IF EXISTS `kompetensi_keahlian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kompetensi_keahlian` (
  `kode_kk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kk` varchar(40) NOT NULL,
  PRIMARY KEY (`kode_kk`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kompetensi_keahlian`
--

LOCK TABLES `kompetensi_keahlian` WRITE;
/*!40000 ALTER TABLE `kompetensi_keahlian` DISABLE KEYS */;
INSERT INTO `kompetensi_keahlian` VALUES (2,'Semua'),(5,'Rekayasa Perangkat Lunak'),(7,'Teknik Komputer & Jaringan');
/*!40000 ALTER TABLE `kompetensi_keahlian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mata_diklat`
--

DROP TABLE IF EXISTS `mata_diklat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mata_diklat` (
  `kode_mata_diklat` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kategori` int(11) NOT NULL,
  `nama_mata_diklat` varchar(40) NOT NULL,
  `kode_kk` int(11) NOT NULL,
  `kkm` int(11) NOT NULL,
  PRIMARY KEY (`kode_mata_diklat`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mata_diklat`
--

LOCK TABLES `mata_diklat` WRITE;
/*!40000 ALTER TABLE `mata_diklat` DISABLE KEYS */;
INSERT INTO `mata_diklat` VALUES (1,1,'Matematika',2,80),(2,1,'Bahasa Indonesia',2,76),(3,1,'Bahasa Inggris',2,80),(5,2,'Pemrograman Web Dinamis',5,87),(6,2,'Pemrograman Desktop',5,87),(7,3,'Bahasa Bali',2,80),(10,2,'Cisco',7,90);
/*!40000 ALTER TABLE `mata_diklat` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `hapusSk` BEFORE DELETE ON `mata_diklat` FOR EACH ROW DELETE FROM standar_kompetensi WHERE standar_kompetensi.kode_mata_diklat = OLD.kode_mata_diklat */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `nilai`
--

DROP TABLE IF EXISTS `nilai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nilai` (
  `nisn` varchar(5) NOT NULL,
  `nip` varchar(5) NOT NULL,
  `kode_sk` int(11) NOT NULL,
  `nilai_angka` int(11) NOT NULL,
  `nilai_huruf` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nilai`
--

LOCK TABLES `nilai` WRITE;
/*!40000 ALTER TABLE `nilai` DISABLE KEYS */;
INSERT INTO `nilai` VALUES ('22791','66004',5,80,'B'),('22792','66004',5,80,'B'),('22793','66004',5,78,'C'),('22794','66004',5,90,'A'),('22795','66004',5,70,'C'),('22791','66004',6,90,'A'),('22792','66004',6,90,'A'),('22793','66004',6,90,'A'),('22794','66004',6,90,'A'),('22795','66004',6,98,'A'),('22791','66002',7,89,'B'),('22792','66002',7,78,'C'),('22793','66002',7,90,'A'),('22794','66002',7,89,'B'),('22795','66002',7,88,'B'),('22791','66002',8,80,'B'),('22792','66002',8,80,'B'),('22793','66002',8,87,'B'),('22794','66002',8,86,'B'),('22795','66002',8,79,'C'),('22791','66003',3,80,'B'),('22792','66003',3,90,'A'),('22793','66003',3,88,'B'),('22794','66003',3,87,'B'),('22795','66003',3,90,'A'),('22791','66003',4,90,'A'),('22792','66003',4,90,'A'),('22793','66003',4,87,'B'),('22794','66003',4,88,'B'),('22795','66003',4,90,'A'),('22791','66006',9,89,'B'),('22792','66006',9,88,'B'),('22793','66006',9,86,'B'),('22794','66006',9,90,'A'),('22795','66006',9,98,'A'),('22791','66006',10,100,'A'),('22792','66006',10,90,'A'),('22793','66006',10,99,'A'),('22794','66006',10,88,'B'),('22795','66006',10,87,'B'),('22791','66001',11,95,'A'),('22792','66001',11,98,'A'),('22793','66001',11,98,'A'),('22794','66001',11,97,'A'),('22795','66001',11,97,'A'),('22791','66001',12,98,'A'),('22792','66001',12,80,'B'),('22793','66001',12,78,'C'),('22794','66001',12,90,'A'),('22795','66001',12,97,'A'),('22791','66007',1,80,'B'),('22792','66007',1,80,'B'),('22793','66007',1,80,'B'),('22794','66007',1,80,'B'),('22795','66007',1,80,'B'),('22791','66007',2,78,'C'),('22792','66007',2,90,'A'),('22793','66007',2,99,'A'),('22794','66007',2,80,'B'),('22795','66007',2,76,'C'),('22791','66007',13,90,'A'),('22792','66007',13,90,'A'),('22793','66007',13,90,'A'),('22794','66007',13,90,'A'),('22795','66007',13,90,'A'),('22791','66007',14,80,'B'),('22792','66007',14,76,'C'),('22793','66007',14,88,'B'),('22794','66007',14,99,'A'),('22795','66007',14,78,'C'),('22796','66004',5,95,'A'),('22797','66004',5,95,'A'),('22796','66004',6,87,'B'),('22797','66004',6,78,'C'),('22796','66002',7,80,'B'),('22797','66002',7,80,'B'),('22796','66002',8,87,'B'),('22797','66002',8,88,'B'),('22796','66003',3,87,'B'),('22797','66003',3,87,'B'),('22796','66003',4,87,'B'),('22797','66003',4,87,'B'),('22796','66003',3,88,'B'),('22797','66003',3,87,'B'),('22796','66007',13,80,'B'),('22797','66007',13,89,'B'),('22796','66007',14,88,'B'),('22797','66007',14,90,'A'),('22796','66008',15,90,'A'),('22797','66008',15,90,'A'),('22796','66008',16,97,'A'),('22797','66008',16,97,'A');
/*!40000 ALTER TABLE `nilai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siswa`
--

DROP TABLE IF EXISTS `siswa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siswa` (
  `nisn` varchar(5) NOT NULL,
  `kode_kk` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `alamat_siswa` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `url_foto` text NOT NULL,
  `nama_foto` text NOT NULL,
  `user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `user_type` enum('siswa','','','') NOT NULL DEFAULT 'siswa',
  `kode_wali` int(11) NOT NULL,
  `kode_kelas` int(11) NOT NULL,
  PRIMARY KEY (`nisn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siswa`
--

LOCK TABLES `siswa` WRITE;
/*!40000 ALTER TABLE `siswa` DISABLE KEYS */;
INSERT INTO `siswa` VALUES ('22791',5,'Januar Kresnamurti','Penamparan','1997-01-13','http://localhost/22791/app/uploads/foto_22791.jpg','foto_22791.jpg','januar','januar','siswa',3,1),('22792',5,'Agnes Rai Madalena','Dalung','1997-03-11','http://localhost/22791/app/uploads/foto_22792.jpg','foto_22792.jpg','agnes','agnes','siswa',4,1),('22793',5,'Ary Aviantara','Pemogan','1996-03-12','http://localhost/22791/app/uploads/foto_22793.jpg','foto_22793.jpg','ary','ary','siswa',1,1),('22794',5,'Abdul Haq','Tabanan','1996-03-21','http://localhost/22791/app/uploads/foto_22794.jpg','foto_22794.jpg','abdul','abdul','siswa',5,1),('22795',5,'Bayu Kresnawan','Jl. Surapati Denpasar','1997-03-04','http://localhost/22791/app/uploads/foto_22795.jpg','foto_22795.jpg','bayukresnawan','bayukresnawan','siswa',6,1),('22796',7,'Yoga Semara','Tabanan','1997-03-12','http://localhost/22791/app/uploads/foto_22796.jpg','foto_22796.jpg','yogasemara','yogasemara','siswa',4,4),('22797',7,'Indra Rudita','Jl. Merpati','1997-03-19','http://localhost/22791/app/uploads/foto_22797.jpg','foto_22797.jpg','indrarud','indrarud','siswa',4,4);
/*!40000 ALTER TABLE `siswa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standar_kompetensi`
--

DROP TABLE IF EXISTS `standar_kompetensi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standar_kompetensi` (
  `kode_sk` int(11) NOT NULL AUTO_INCREMENT,
  `kode_mata_diklat` int(11) NOT NULL,
  `nama_sk` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_sk`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standar_kompetensi`
--

LOCK TABLES `standar_kompetensi` WRITE;
/*!40000 ALTER TABLE `standar_kompetensi` DISABLE KEYS */;
INSERT INTO `standar_kompetensi` VALUES (3,2,'Surat'),(4,2,'Puisi'),(5,3,'Gerund'),(6,3,'Conditional Sentence'),(7,1,'Limit & Turunan'),(8,1,'Parabola'),(9,6,'Pemrograman Visual Basic Dasar'),(10,6,'Pemrograman Visual Basic Lanjut'),(11,5,'Pemrograman PHP & MySQL'),(12,5,'Integerasi Server Dengan Program PHP'),(13,7,'Nyurat Lontar'),(14,7,'Mekidung'),(15,10,'Cisco Dasar'),(16,10,'Cisco Lanjut');
/*!40000 ALTER TABLE `standar_kompetensi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `kode_user` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `user_type` enum('admin') NOT NULL,
  PRIMARY KEY (`kode_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'dewadg','skensajaya','admin');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wali_murid`
--

DROP TABLE IF EXISTS `wali_murid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wali_murid` (
  `kode_wali` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ayah` varchar(50) NOT NULL,
  `pekerjaan_ayah` varchar(30) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `pekerjaan_ibu` varchar(30) NOT NULL,
  `alamat_wali` text NOT NULL,
  `telp_wali` varchar(20) NOT NULL,
  PRIMARY KEY (`kode_wali`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wali_murid`
--

LOCK TABLES `wali_murid` WRITE;
/*!40000 ALTER TABLE `wali_murid` DISABLE KEYS */;
INSERT INTO `wali_murid` VALUES (1,'Agung Purbayana','Dosen','Ferawati','-','Dalung','085935355888'),(3,'Anom Setiawan','Wiraswasta','Suwendri','-','Penamparan','087657545654'),(4,'Rudita','Wiraswasta','Darmini','Wiraswasta','Jl. Merpati','085935355888'),(5,'Muhammad Amin','Pedagang','Rini','Pedagang','Tabanan','0878768787689'),(6,'Sugita','Dosen','Gandawati','-','Jl. Surapati Denpasar','087676545654');
/*!40000 ALTER TABLE `wali_murid` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-10 13:21:33
